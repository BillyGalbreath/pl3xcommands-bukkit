package net.pl3x.bukkit.pl3xcommands;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import net.pl3x.bukkit.pl3xcommands.commands.Admin;
import net.pl3x.bukkit.pl3xcommands.commands.Back;
import net.pl3x.bukkit.pl3xcommands.commands.Biome;
import net.pl3x.bukkit.pl3xcommands.commands.Broadcast;
import net.pl3x.bukkit.pl3xcommands.commands.Buddha;
import net.pl3x.bukkit.pl3xcommands.commands.Burn;
import net.pl3x.bukkit.pl3xcommands.commands.Clear;
import net.pl3x.bukkit.pl3xcommands.commands.ClearInventory;
import net.pl3x.bukkit.pl3xcommands.commands.Compass;
import net.pl3x.bukkit.pl3xcommands.commands.Coords;
import net.pl3x.bukkit.pl3xcommands.commands.Deafen;
import net.pl3x.bukkit.pl3xcommands.commands.DelJail;
import net.pl3x.bukkit.pl3xcommands.commands.DelWarp;
import net.pl3x.bukkit.pl3xcommands.commands.Enchant;
import net.pl3x.bukkit.pl3xcommands.commands.EnchantingTable;
import net.pl3x.bukkit.pl3xcommands.commands.Entities;
import net.pl3x.bukkit.pl3xcommands.commands.Explode;
import net.pl3x.bukkit.pl3xcommands.commands.Extinguish;
import net.pl3x.bukkit.pl3xcommands.commands.Facepalm;
import net.pl3x.bukkit.pl3xcommands.commands.FakeOp;
import net.pl3x.bukkit.pl3xcommands.commands.Fart;
import net.pl3x.bukkit.pl3xcommands.commands.Feed;
import net.pl3x.bukkit.pl3xcommands.commands.Fireball;
import net.pl3x.bukkit.pl3xcommands.commands.FixChunk;
import net.pl3x.bukkit.pl3xcommands.commands.Fly;
import net.pl3x.bukkit.pl3xcommands.commands.FlySpeed;
import net.pl3x.bukkit.pl3xcommands.commands.Freeze;
import net.pl3x.bukkit.pl3xcommands.commands.GameMode;
import net.pl3x.bukkit.pl3xcommands.commands.GetIP;
import net.pl3x.bukkit.pl3xcommands.commands.Global;
import net.pl3x.bukkit.pl3xcommands.commands.God;
import net.pl3x.bukkit.pl3xcommands.commands.Harm;
import net.pl3x.bukkit.pl3xcommands.commands.Hat;
import net.pl3x.bukkit.pl3xcommands.commands.Heal;
import net.pl3x.bukkit.pl3xcommands.commands.Ignore;
import net.pl3x.bukkit.pl3xcommands.commands.InvMod;
import net.pl3x.bukkit.pl3xcommands.commands.Jail;
import net.pl3x.bukkit.pl3xcommands.commands.Jump;
import net.pl3x.bukkit.pl3xcommands.commands.KittyCannon;
import net.pl3x.bukkit.pl3xcommands.commands.Local;
import net.pl3x.bukkit.pl3xcommands.commands.Lore;
import net.pl3x.bukkit.pl3xcommands.commands.Mail;
import net.pl3x.bukkit.pl3xcommands.commands.Message;
import net.pl3x.bukkit.pl3xcommands.commands.Mute;
import net.pl3x.bukkit.pl3xcommands.commands.MuteAll;
import net.pl3x.bukkit.pl3xcommands.commands.Nick;
import net.pl3x.bukkit.pl3xcommands.commands.Ping;
import net.pl3x.bukkit.pl3xcommands.commands.Pl3xCommands;
import net.pl3x.bukkit.pl3xcommands.commands.PowerTool;
import net.pl3x.bukkit.pl3xcommands.commands.Quit;
import net.pl3x.bukkit.pl3xcommands.commands.RageQuit;
import net.pl3x.bukkit.pl3xcommands.commands.RealName;
import net.pl3x.bukkit.pl3xcommands.commands.Rename;
import net.pl3x.bukkit.pl3xcommands.commands.Repair;
import net.pl3x.bukkit.pl3xcommands.commands.Reply;
import net.pl3x.bukkit.pl3xcommands.commands.Ride;
import net.pl3x.bukkit.pl3xcommands.commands.Rocket;
import net.pl3x.bukkit.pl3xcommands.commands.Seen;
import net.pl3x.bukkit.pl3xcommands.commands.SetJail;
import net.pl3x.bukkit.pl3xcommands.commands.SetSpawn;
import net.pl3x.bukkit.pl3xcommands.commands.SetWarp;
import net.pl3x.bukkit.pl3xcommands.commands.Smite;
import net.pl3x.bukkit.pl3xcommands.commands.Spawn;
import net.pl3x.bukkit.pl3xcommands.commands.SpawnMob;
import net.pl3x.bukkit.pl3xcommands.commands.Speak;
import net.pl3x.bukkit.pl3xcommands.commands.Spy;
import net.pl3x.bukkit.pl3xcommands.commands.Starve;
import net.pl3x.bukkit.pl3xcommands.commands.Sudo;
import net.pl3x.bukkit.pl3xcommands.commands.Suicide;
import net.pl3x.bukkit.pl3xcommands.commands.Time;
import net.pl3x.bukkit.pl3xcommands.commands.Top;
import net.pl3x.bukkit.pl3xcommands.commands.TpAccept;
import net.pl3x.bukkit.pl3xcommands.commands.TpDeny;
import net.pl3x.bukkit.pl3xcommands.commands.TpToggle;
import net.pl3x.bukkit.pl3xcommands.commands.Tpa;
import net.pl3x.bukkit.pl3xcommands.commands.TpaAll;
import net.pl3x.bukkit.pl3xcommands.commands.TpaHere;
import net.pl3x.bukkit.pl3xcommands.commands.Universe;
import net.pl3x.bukkit.pl3xcommands.commands.WalkSpeed;
import net.pl3x.bukkit.pl3xcommands.commands.Warp;
import net.pl3x.bukkit.pl3xcommands.commands.Weather;
import net.pl3x.bukkit.pl3xcommands.commands.Workbench;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.hook.Vault;

public class Main extends JavaPlugin {
	private static Main instance;

	public static Main getInstance() {
		return instance;
	}

	public void onEnable() {
		instance = this;

		saveDefaultConfig();

		Vault.setupChat();

		init();

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			Logger.log("&4Failed to start Metrics: &e" + e.getMessage());
		}

		Logger.log(getName() + " v" + getDescription().getVersion() + " enabled!");
	}

	public void onDisable() {
		disable();

		Logger.log(getName() + " disabled!");
	}

	private void init() {
		new Admin(this);
		new Back(this);
		new Biome(this);
		new Broadcast(this);
		new Buddha(this);
		new Burn(this);
		new Clear(this);
		new ClearInventory(this);
		new Compass(this);
		new Coords(this);
		new Deafen(this);
		// new DelHome(this);
		new DelJail(this);
		new DelWarp(this);
		new Enchant(this);
		new EnchantingTable(this);
		new Entities(this);
		new Explode(this);
		new Extinguish(this);
		new Facepalm(this);
		new FakeOp(this);
		new Fart(this);
		new Feed(this);
		new Fireball(this);
		new FixChunk(this);
		new Fly(this);
		new FlySpeed(this);
		new Freeze(this);
		new GameMode(this);
		new GetIP(this);
		new Global(this);
		new God(this);
		new Harm(this);
		new Hat(this);
		new Heal(this);
		// new Home(this);
		new Ignore(this);
		new InvMod(this);
		new Jail(this);
		new Jump(this);
		new KittyCannon(this);
		// new ListHome(this);
		new Local(this);
		new Lore(this);
		new Mail(this);
		new Message(this);
		new Mute(this);
		new MuteAll(this);
		new Nick(this);
		new Ping(this);
		new Pl3xCommands(this);
		new PowerTool(this);
		new Quit(this);
		new RageQuit(this);
		new RealName(this);
		new Rename(this);
		new Repair(this);
		new Reply(this);
		new Ride(this);
		new Rocket(this);
		new Seen(this);
		// new SetHome(this);
		new SetJail(this);
		new SetSpawn(this);
		new SetWarp(this);
		new Smite(this);
		new Spawn(this);
		new SpawnMob(this);
		new Speak(this);
		new Spy(this);
		new Starve(this);
		new Sudo(this);
		new Suicide(this);
		new Time(this);
		new Top(this);
		new Tpa(this);
		new TpaAll(this);
		new TpAccept(this);
		new TpaHere(this);
		new TpDeny(this);
		new TpToggle(this);
		new Universe(this);
		new WalkSpeed(this);
		new Warp(this);
		new Weather(this);
		new Workbench(this);
	}

	private void disable() {
		// cancel all tasks
		getServer().getScheduler().cancelTasks(this);

		// unregister all listeners
		for (RegisteredListener listener : HandlerList.getRegisteredListeners(this)) {
			try {
				((HandlerList) listener.getClass().getMethod("getHandlerList").invoke(null)).unregister(listener);
			} catch (Exception ignore) {
			}
		}

		// unregister all commands
		// not needed?
	}

	public void reload() {
		disable();
		init();
	}

	public boolean registerCommand(CommandExecutor executor, String command) {
		if (Config.DISABLED_COMMANDS.getStringList().contains(command.toLowerCase())) {
			getCommand(command).setExecutor(this);
			return false;
		}
		getCommand(command).setExecutor(executor);
		return true;
	}

	public void sendMessage(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	public void sendBroadcast(String message) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			sendMessage(player, message);
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		sendMessage(sender, Lang.ERROR_COMMAND_DISABLED.get());
		return true;
	}
}
