package net.pl3x.bukkit.pl3xcommands;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.commands.Spy;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.hook.Vault;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ChatManager {
	private static ChatManager manager;
	private HashMap<UUID, Channel> playerChannels = new HashMap<UUID, Channel>();

	public static ChatManager getManager() {
		if (manager == null) {
			manager = new ChatManager();
		}
		return manager;
	}

	public void setChannel(Player player, Channel channel) {
		playerChannels.put(player.getUniqueId(), channel);
	}

	public Channel getChannel(Player player) {
		Channel channel = playerChannels.get(player.getUniqueId());
		if (channel == null) {
			try {
				channel = Channel.valueOf(Config.DEFAULT_CHAT_CHANNEL.getString().toUpperCase());
			} catch (Exception e) {
				channel = Channel.LOCAL;
			}
		}
		return channel;
	}

	public String sanitizeInput(String str) {
		str = str.replace("%", "%%");
		return str;
	}

	public String replaceVariables(Player player, String message) {
		Channel channel = getChannel(player);
		String format = getChatFormat(channel);

		format = format.replace("{prefix}", Vault.getGroupPrefix(player));
		format = format.replace("{suffix}", Vault.getGroupSuffix(player));
		format = format.replace("{group}", Vault.getPrimaryGroup(player));
		format = format.replace("{username}", player.getName());
		format = format.replace("{displayname}", player.getDisplayName());
		format = format.replace("{world}", player.getWorld().getName());
		format = format.replace("{timestamp}", getTimeStamp());

		format = ChatColor.translateAlternateColorCodes('&', format);

		format = format.replace("{message}", message);

		return format;
	}

	public String getChatFormat(Channel channel) {
		switch (channel) {
			case ADMIN:
				return Config.CHAT_ADMIN_FORMAT.getString();
			case UNIVERSE:
				return Config.CHAT_UNIVERSE_FORMAT.getString();
			case GLOBAL:
				return Config.CHAT_GLOBAL_FORMAT.getString();
			case LOCAL:
			default:
				return Config.CHAT_LOCAL_FORMAT.getString();
		}
	}

	public String getTimeStamp() {
		String format = Config.CHAT_TIMESTAMP_FORMAT.getString();
		return new SimpleDateFormat(format).format(new Date());
	}

	public Set<Player> getRecipients(Player player) {
		Set<Player> recipients = new HashSet<Player>();
		Channel channel = getChannel(player);
		switch (channel) {
			case ADMIN:
				for (Player target : Bukkit.getOnlinePlayers()) {
					if (target.getUniqueId().equals(player.getUniqueId())) {
						continue;
					}
					if (!target.hasPermission("command.admin")) {
						continue;
					}
					recipients.add(target);
				}
				if (recipients.isEmpty()) {
					delayMessage(player, Lang.ADMIN_NO_ONE_HEARS_YOU.get(), 10);
				}
				break;
			case GLOBAL:
				World world = player.getWorld();
				for (Player target : Bukkit.getOnlinePlayers()) {
					if (target.getUniqueId().equals(player.getUniqueId())) {
						continue;
					}
					if (!target.getWorld().equals(world)) {
						continue;
					}
					recipients.add(target);
				}
				if (recipients.isEmpty()) {
					delayMessage(player, Lang.GLOBAL_NO_ONE_HEARS_YOU.get(), 10);
				}
				break;
			case UNIVERSE:
				for (Player target : Bukkit.getOnlinePlayers()) {
					if (target.getUniqueId().equals(player.getUniqueId())) {
						continue;
					}
					recipients.add(target);
				}
				if (recipients.isEmpty()) {
					delayMessage(player, Lang.UNIVERSE_NO_ONE_HEARS_YOU.get(), 10);
				}
				break;
			case LOCAL:
			default:
				int radius = Config.LOCAL_CHAT_RANGE.getInt();
				radius = radius * radius;
				Location location = player.getLocation();
				for (Player target : Bukkit.getOnlinePlayers()) {
					if (target.getUniqueId().equals(player.getUniqueId())) {
						continue;
					}
					Location tLoc = target.getLocation();
					if (!tLoc.getWorld().equals(location.getWorld())) {
						continue;
					}
					if (tLoc.distanceSquared(location) > radius) {
						continue;
					}
					recipients.add(target);
				}
				if (recipients.isEmpty()) {
					delayMessage(player, Lang.LOCAL_NO_ONE_HEARS_YOU.get(), 10);
				}
		}
		recipients.add(player); // always add self
		return recipients;
	}

	public Set<Player> getSpies(Set<Player> recipients) {
		Set<Player> spies = new HashSet<Player>();
		for (Player target : Bukkit.getOnlinePlayers()) {
			if (recipients.contains(target)) {
				continue;
			}
			if (!Spy.isSpy(target)) {
				continue;
			}
			spies.add(target);
		}
		return spies;
	}

	public String getSpyMessage(Player player, String message) {
		String format = Config.CHAT_SPY_FORMAT.getString();

		format = format.replace("{prefix}", Vault.getGroupPrefix(player));
		format = format.replace("{suffix}", Vault.getGroupSuffix(player));
		format = format.replace("{group}", Vault.getPrimaryGroup(player));
		format = format.replace("{username}", player.getName());
		format = format.replace("{displayname}", player.getDisplayName());
		format = format.replace("{world}", player.getWorld().getName());
		format = format.replace("{timestamp}", getTimeStamp());

		format = ChatColor.translateAlternateColorCodes('&', format);

		format = format.replace("{message}", message);

		return format;
	}

	public void delayMessage(Player player, String message, int delay) {
		new DelayedMessage(player, message).runTaskLater(Main.getInstance(), delay);
	}

	public class DelayedMessage extends BukkitRunnable {
		private Player player;
		private String message;

		public DelayedMessage(Player player, String message) {
			this.player = player;
			this.message = message;
		}

		@Override
		public void run() {
			Main.getInstance().sendMessage(player, message);
		}
	}

	public enum Channel {
		LOCAL,
		GLOBAL,
		UNIVERSE,
		ADMIN
	}
}
