package net.pl3x.bukkit.pl3xcommands.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class PlayerConfig extends YamlConfiguration {
	private static Map<UUID, PlayerConfig> configs = new HashMap<UUID, PlayerConfig>();

	public static PlayerConfig getConfig(Player player) {
		return getConfig(player.getUniqueId());
	}

	public static PlayerConfig getConfig(OfflinePlayer player) {
		UUID uuid = player.getUniqueId();
		if (!new File(Main.getInstance().getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml").exists()) {
			return null;
		}
		return getConfig(uuid);
	}

	public static PlayerConfig getConfig(UUID uuid) {
		synchronized (configs) {
			if (configs.containsKey(uuid)) {
				return configs.get(uuid);
			}
			PlayerConfig config = new PlayerConfig(uuid);
			configs.put(uuid, config);
			return config;
		}
	}

	public static void removeConfigs() {
		Collection<PlayerConfig> oldConfs = new ArrayList<PlayerConfig>(configs.values());
		synchronized (configs) {
			for (PlayerConfig config : oldConfs) {
				config.discard();
			}
		}
	}

	private File file = null;
	private Object saveLock = new Object();
	private UUID uuid;

	public PlayerConfig(UUID uuid) {
		super();
		file = new File(Main.getInstance().getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml");
		this.uuid = uuid;
		load();
	}

	@SuppressWarnings("unused")
	private PlayerConfig() {
		uuid = null;
	}

	private void load() {
		synchronized (saveLock) {
			try {
				this.load(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void forceSave() {
		synchronized (saveLock) {
			try {
				save(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void discard() {
		forceSave();
		synchronized (configs) {
			configs.remove(uuid);
		}
	}

	public Location getLocation(String path) {
		if (get(path) == null) {
			return null;
		}
		World world = Bukkit.getWorld(getString(path + ".world", ""));
		if (world == null) {
			return null;
		}
		double x = getDouble(path + ".x");
		double y = getDouble(path + ".y");
		double z = getDouble(path + ".z");
		float pitch = (float) getDouble(path + ".pitch");
		float yaw = (float) getDouble(path + ".yaw");
		return new Location(world, x, y, z, yaw, pitch);
	}

	public void setLocation(String path, Location value) {
		set(path + ".world", value.getWorld().getName());
		set(path + ".x", value.getX());
		set(path + ".y", value.getY());
		set(path + ".z", value.getZ());
		set(path + ".pitch", value.getPitch());
		set(path + ".yaw", value.getYaw());
		forceSave();
	}

	public Inventory getBackpack() {
		Integer size = getInt("backpack.size");
		if (size == null || size < 9 || size % 9 != 0) {
			size = 36;
		}
		Player player = Bukkit.getPlayer(uuid);
		Inventory inventory = Bukkit.createInventory(player, size, "Backpack");
		if (get("backpack.item") == null) {
			return inventory;
		}
		for (int slot = 0; slot < size; slot++) {
			inventory.setItem(slot, getItemStack("backpack.item." + slot));
		}
		return inventory;
	}

	public void setBackpack(Inventory inventory) {
		for (int slot = 0; slot < inventory.getSize(); slot++) {
			set("backpack.item." + slot, inventory.getItem(slot));
		}
		set("backpack.size", inventory.getSize());
		forceSave();
	}
}
