package net.pl3x.bukkit.pl3xcommands.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

public class WarpConfig extends YamlConfiguration {
	private static WarpConfig config;

	public static WarpConfig getConfig() {
		if (config == null) {
			config = new WarpConfig();
		}
		return config;
	}

	private File file = null;
	private Object saveLock = new Object();

	public WarpConfig() {
		super();
		file = new File(Main.getInstance().getDataFolder(), "warps.yml");
		load();
	}

	private void load() {
		synchronized (saveLock) {
			try {
				this.load(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void forceSave() {
		synchronized (saveLock) {
			try {
				save(file);
			} catch (Exception ignore) {
			}
		}
	}

	public List<String> getWarps() {
		List<String> warps = new ArrayList<String>();
		if (!isSet("warps")) {
			return warps;
		}
		for (String warp: getConfigurationSection("warps").getValues(false).keySet()) {
			warps.add(warp.toLowerCase());
		}
		return warps;
	}

	public Location getWarp(String warp) {
		double x = getDouble("warps." + warp + ".x");
		double y = getDouble("warps." + warp + ".y");
		double z = getDouble("warps." + warp + ".z");
		World world = Bukkit.getWorld(getString("warps." + warp + ".world", ""));
		float pitch = (float) getDouble("warps." + warp + ".pitch");
		float yaw = (float) getDouble("warps." + warp + ".yaw");
		if (world == null) {
			return null;
		}
		return new Location(world, x, y, z, yaw, pitch);
	}

	public void setWarp(String warp, Location location) {
		set("warps." + warp + ".x", location.getX());
		set("warps." + warp + ".y", location.getY());
		set("warps." + warp + ".z", location.getZ());
		set("warps." + warp + ".world", location.getWorld().getName());
		set("warps." + warp + ".pitch", location.getPitch());
		set("warps." + warp + ".yaw", location.getYaw());
		forceSave();
	}

	public void deleteWarp(String warp) {
		set("warps." + warp, null);
		forceSave();
	}
}
