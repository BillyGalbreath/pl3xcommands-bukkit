package net.pl3x.bukkit.pl3xcommands.configuration;

import java.io.File;

import net.pl3x.bukkit.pl3xcommands.Logger;
import net.pl3x.bukkit.pl3xcommands.Main;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Lang {
	ERROR_COMMAND_DISABLED("&4That command is disabled!"),
	ERROR_PLAYER_COMMAND("&4This command is only available to players."),
	ERROR_COMMAND_NO_PERMISSION("&4You do not have permission for that command!"),
	ERROR_PLAYER_NOT_ONLINE("&4That player is not online!"),
	ERROR_PLAYER_NOT_FOUND("&4That player does not exist!"),
	ERROR_WORLD_DOES_NOT_EXIST("&4World does not exist!"),
	ERROR_NOTHING_IN_HAND("&4Nothing is in your hand!"),
	ERROR_NOT_VALID_NUMBER("&4Not a valid number!"),
	ERROR_NUMBER_NOT_BETWEEN("&4Value must be between &e{number1}&4 and &7{number2}&4!"),
	ERROR_NOMBER_NOT_POSITIVE("&4The value must be a positive number!"),
	ERROR_NUNBER_TOO_BIG("&4The value specified is larger than allowed."),

	ADMIN_NO_ONE_HEARS_YOU("&e&oNo other admins are online to hear you. Forever alone. :'("),

	BACK_NOT_FOUND("&4You have no place to go back to!"),
	BACK_TELEPORTING("&dReturning to your previous location."),
	BACK_DEATH_HINT("&dType &7/back &d to go back to where you died."),

	BACKPACK_CANNOT_ACCESS("&4You cannot access that players backpack!"),
	BACKPACK_NOT_FOUND("&4Cannot locate backpack!"),

	BIOME_CURRENT("&dCurrent biome&e: &7{biome}"),
	BIOME_LIST("&dBiomes&e: &7{list}"),
	BIOME_NOT_FOUND("&4Biome does not exist!"),
	BIOME_SET("&dBiome has been set."),

	BUDDHA_ENABLED("&dEnabled buddha mode for yourself."),
	BUDDHA_DISABLED("&dDisabled buddha mode for yourself."),
	BUDDHA_ENABLED_BY("&dBuddha mode enabled by &7{player}&d."),
	BUDDHA_DISABLED_BY("&dBuddha mode disabled by &7{player}&d."),
	BUDDHA_ENABLED_FOR("&dBuddha mode enabled for &7{player}&d."),
	BUDDHA_DISABLED_FOR("&dBuddha mode disabled for &7{player}&d."),

	BURN_EXEMPT("&4You cannot burn that player!"),
	BURN("&dYou have set &7{player} &don fire for &7{time}&d."),

	CLEAR("&dCleared &7{count} {type}&d."),
	CLEAR_LIST("&dEntity types: &7{list}"),
	CLEAR_NONE_NEAR("&dNo entities nearby to clear."),
	CLEAR_UNKNOWN_TYPE("&4Unknown entity type!"),

	CLEARINVENTORY_EXEMPT("&4You cannot alter that players inventory!"),
	CLEARINVENTORY("&dYou have cleared your inventory."),
	CLEARINVENTORY_NOTICE("&4Your inventory has been cleared."),
	CLEARINVENTORY_OTHER("&dYou have cleared the inventory of &7{player}&d."),

	COMPASS_CURRENT_POSITION("&dYour compass now points to your current location."),
	COMPASS_INVALID_COORDS("&4Those coordinates are invalid!"),
	COMPASS_POINT_PLAYER("&dYour compass now points towards &7{player}&d."),
	COMPASS_POINT_COORDS("&dYour compass now points towards &7{x}&d, &7{y}&d, &7{z}&d."),
	COMPASS_RESET("&dReset your compass."),

	COORDS("&dCoords: &7{world} &e[&7{x}&d, &7{y}&d, &7{z}&e]"),

	DEAFEN_EXEMPT("&4You cannot deafen that player!"),
	DEAFEN_ON("&dYou have deafened &7{player}&d."),
	DEAFEN_OFF("&dYou have undeafened &7{player}&d."),

	ENCHANT_REMOVED("&dRemoved all enchantments from &7{item}&d."),
	ENCHANT_LEVEL_TOO_HIGH("&4Enchant level is above allowed value!"),
	ENCHANT_ADDED("&dAdded &7{enchantment} &dlevel &7{level} &dto &7{item}&d."),
	ENCHANT_ADDED_ALL("&dAdded &7all &dlevel &7{level} to &7{item}&d."),
	ENCHANT_NOT_FOUND("&4No such enchantment!"),
	ENCHANT_LIST("&dEnchantments: &7{list}"),
	ENCHANT_ILLEGAL_ITEM("&4Cannot enchant that item!"),
	ENCHANT_CONFLICT("&4Cannot add &7{enchantment} &4because it conflicts with &7{conflict}&4!"),

	ENCHANTING_TABLE_OPENED("&dOpened an enchanting table for you."),

	ENTITIES_DISTANCE("&7{entity}&d: &e{distance}"),
	ENTITIES_TOTAL("&7{total} &dtotal entities nearby."),
	ENTITIES_NONE("&dNo entities nearby."),

	EXPLODE_EXEMPT("&4You cannot explode that player!"),
	EXPLODE_PLAYER("&d4You has exploded &7{player}&d."),

	EXTINGUISH_BY("&dYou have been extinguished by &7{player}&d."),
	EXTINGUISH_FOR("&dYou have extinguished &7{player}&d."),
	EXTINGUISH("&dYou have been extinguished."),

	FACEPALM_EXEMPT("&4You cannot facepalm at that player!"),
	FACEPALM("&e{player} &3has facepalmed."),
	FACEPALM_AT("&e{player} &3has facepalmed at &e{target}&3."),

	FAKEOP("&7{target} &dhas been sent a fake op notice."),
	FAKEOP_VICTIM("&eYou are now op!"),

	FEED("&dYou have fed yourself!"),
	FEED_OTHER("&dYou have fed &7{player}&d!"),
	FEED_NOTICE("&You have been fed by &7{player} &d!"),

	FIXCHUNK("&dThe chunk you are standing in has been refreshed."),
	FIXCHUNK_ERROR("&4The chunk could not be refreshed!"),

	FLY("&dToggled flight &7{action}&d."),
	FLY_OTHER("&dToggled flight &7{action}&d on &7{player}&d."),
	FLY_NOTICE("&dYou've had flight toggled &7{action}&d."),
	FLY_EXEMPT("&4You cannot toggle that player's fly!"),

	FLYSPEED("&dYour flight speed is now set to &7{speed}&d."),
	FLYSPEED_EXEMPT("&4You cannot change that player's fly speed!"),

	FREEZE_EXEMPT("&4You cannot freeze that player!"),
	FREEZE("&dYou have {action} &7{player}&d!"),
	FREEZE_NOTICE("&dYou have been &7{action}&d!"),
	FROZEN("&4You are frozen."),

	GAMEMODE_EXEMPT("&4You may not change that players gamemode."),
	GAMEMODE_UNKNOWN("&4Invalid gamemode!"),
	GAMEMODE_SET("&dYour game mode has been set to &7{gamemode}&d."),
	GAMEMODE_SET_OTHER("&dGamemode for &7{player} &dis now set to &7{gamemode}&d."),

	GETIP("&7{player}&d: &7{ip}"),

	GLOBAL_NO_ONE_HEARS_YOU("&e&oYou seem to be alone on this planet. Try using /universe chat."),

	GOD_ENABLED("&dEnabled god mode for yourself."),
	GOD_DISABLED("&dDisabled god mode for yourself."),
	GOD_ENABLED_BY("&dGod mode enabled by &7{player}&d."),
	GOD_DISABLED_BY("&dGod mode disabled by &7{player}&d."),
	GOD_ENABLED_FOR("&dGod mode enabled for &7{player}&d."),
	GOD_DISABLED_FOR("&dGod mode disabled for &7{player}&d."),

	HARM("&dYou just damaged &7{player}&d!"),
	HARM_NOTICE("&4You have just been damaged by &7{player}&4!"),
	HARM_EXEMPT("&4You may not harm that player."),

	HAT("&dSet your hat."),
	HAT_FULL_INVENTORY("&4You do not have enough inventory slots to remove your current hat!"),

	HEAL("&dYou have healed yourself!"),
	HEAL_OTHER("&dYou have healed &7{player}&d."),
	HEAL_NOTICE("&dYou have been healed by &7{player}&d."),

	HOME_DELETE_DEFAULT("&4Type &7/delhome home &4to delete your default home."),
	HOME_DELETE("&dThe home &7{home} &dhas been deleted."),
	HOME_DOES_NOT_EXIST("&4That home does not exist!"),
	HOME_LIST("&dHomes:"),
	HOME_LIST_EXEMPT("&4You may not list that players homes."),
	HOME_FOUND_NONE("&4No homes found!"),
	HOME_NO_OTHER("&4You cannot go to that players house!"),
	HOME_OTHER_ERROR("&4You must include the name of the player and home (player:home)."),
	HOME("&dGoing to home &7{home}&d."),
	HOME_DEFAULT("&dGoing home."),
	HOME_INVALID_CHARACTERS("&4The name of your home cannot contain &7:&4!"),
	HOME_NO_MULTI("&4You do not have permission for multiple homes!"),
	HOME_SET("&dHome &7{home} &dset."),
	HOME_SET_DEFAULT("&dHome set."),
	HOME_SET_MAX("&4You have reached your max number of homes! &e(&7{limit}&e)"),

	IGNORE_EXEMPT("&4You may not ignore that player."),
	IGNORE_REMOVE("&dYou have no longer ignoring &7{player}&d."),
	IGNORE_ADD("&dYou are now ignoring &7{player}&d."),
	IGNORE_MESSAGE("&4That user is ignoring you!"),

	INVMOD("&dOpened the inventory of &7{player}&d."),
	INVMOD_EXEMPT("&4You may not modify that players inventory."),

	JAIL_DELETED("&dThe jail &7{jail} &dhas been deleted."),
	JAIL_NOT_FOUND("&4That jail does not exist!"),
	JAIL_SET("&dJail &7{jail} &dset."),
	JAIL_NONE_FOUND("&4There are no jails!"),
	JAIL_LIST("&dJails: {list}"),
	JAIL_EXEMPT("'&4You may not jail that player."),
	JAILED_RELEASED("&dYou have released &7{player}&d."),
	JAILED_RELEASED_NOTICE("&dYou have been released."),
	JAILED("&dYou have jailed &7{player}&d."),
	JAILED_NOTICE("&4You have been jailed."),
	JAILED_MSG("&4You are jailed."),

	JUMP_NOT_THERE("&4Cannot jump there!"),

	LOCAL_NO_ONE_HEARS_YOU("&e&oNo one but the tumbleweeds hear you. Try using /global chat."),

	LORE("&dLore is set."),
	LORE_RESET("&dLore is reset."),

	MAIL_CLEARED("&dYour mailbox has been cleared."),
	MAIL_ENTRY("&d[&e{from}&d] &e&o{message}"),
	MAIL_NONE("&4You have no mail!"),
	MAIL_SENT("&dYour mail has been sent."),
	MAIL_NOTIFICATION("&dYou have &7{count} &dunread messages in your mailbox."),

	MESSAGE_NULL("&4You entered no message!"),
	MESSAGE_SENDER("&7[&d{sender} &e-> &d{target}&7] &7&o{message}"),
	MESSAGE_TARGET("&7[&d{sender} &e-> &d{target}&7] &7&o{message}"),
	MESSAGE_SPY("&7[&d{sender} &e-> &d{target}&7] &6&o{message}"),
	MESSAGE_NO_REPLY("&4You have no one to reply to!"),

	MUTE_EXEMPT("&4You cannot mute that player!"),
	MUTE_YOURSELF("&4Cannot mute yourself!"),
	MUTE_SET("&dYou have muted &7{player}&d."),
	MUTE_UNSET("&dYou have unmuted &7{player}&d."),
	MUTE_NOTICE("&4You have been muted by &7{player}&4."),
	MUTE_NOTICE_UNMUTE("&4You have been unmuted by &7{player}&4."),
	MUTED_CANT_TALK("&4You are muted and cannot speak for &7{time}&d."),
	MUTEALL("&dYou have muted all players."),
	MUTEALL_UNMUTE("&dYou have unmuted all players."),
	MUTEALL_NOTICE("&4You have been muted!"),
	MUTEALL_NOTICE_UNMUTE("&dYou have been unmuted!"),

	NICK_EXEMPT("&4You cannot change that player's nick!"),
	NICK_SET("&dYour nick is now set to &7{nick}&d."),
	NICK_RESET("&dYour nick is now reset."),
	NICK_SET_OTHER("&dChanged &7{player}&d's nickname to &7{nick}&d."),
	NICK_SET_NOTICE("&dYour nickname was changed to &7{nick}&d."),
	NICK_RESET_OTHER("&dYou reset &7{player}&d's nickname."),
	NICK_RESET_NOTICE("&dYour nickname was reset."),

	PING("&dPong!"),

	PL3XCOMMANDS_RELOAD("&d{name} reloaded."),
	PL3XCOMMANDS_VERSION("&d{name} v{version}."),

	POWERTOOL_REMOVED("&dAll commands removed from item."),
	POWERTOOL_ADDED("&dAdded command to item."),

	QUIT("You have left the game."),
	QUIT_BROADCAST("&7{player} &dhas left the game."),
	QUIT_SHUTDOWN("The server has closed."),
	QUIT_CONSOLE_STOP("Kick all online players before shutting down!"),

	RAGEQUIT_BROADCAST("&7{player} &4has ragequit!"),
	RAGEQUIT_EXEMPT("&4You may not ragequit that player."),
	RAGEQUIT("&4RAAAGGGEEEE!!!"),

	REALNAME("&e{nickname} &d= &7{realname}"),

	RENAME("&dRenamed item to &7{name}&d."),
	RENAME_SHORTENED_NAME("&dThe new name has been shortened to &7{name} &dto prevent crashes."),

	REPAIR_ALL("&4You have nothing to repair!"),
	REPAIR_NOT_NEEDED("&4That does not need to be repaired!"),
	REPAIR("&dItem has been repaired."),

	RIDE_EJECT("&dYou have ejected."),
	RIDE_EXEMPT("&4You may not ride that player."),
	RIDE_SELF("&4You cannot ride yourself."),

	ROCKET("&dYou have rocketed &7{player}&d."),
	ROCKET_EXEMPT("&4You may not rocket that player."),

	SAY("&3[&eserver&3] &d{message}"),

	SEEN("&dThe player &7{player} &dwas last seen &7{time} &dago."),
	SEEN_NOW("&7{player} &dis online right now!"),
	SEEN_UNKNOWN("&4I do not know when that player was last seen!"),

	SIGNEDIT("&dSign edited."),
	SIGNEDIT_NOT_A_SIGN("&4Not a sign!"),

	SMITE_EXEMPT("&4You may not smite that player."),
	SMITE_NOT_THERE("&4Can not smite there!"),
	SMITE_VICTIM("&4You have been smited by &7{player}&4!"),
	SMITE("&dSmiting &7{player}&d!"),

	SPAWN_SET("&dThe spawn point of &7{world} &dis set."),
	SPAWN_NO_CHANGE_WORLD("&4You do not have permission to spawn in other worlds."),
	SPAWN("&dGoing to spawn in &7{world}&d."),

	SPAWNMOB_LIST("&dValid mobs: &7{list}"),
	SPAWNMOB_INVALID("&4Invalid mob!"),
	SPAWNMOB("&dSpawned &7{mob}&d."),

	SPEAK_EXEMPT("&4You may not make that player speak!"),
	SPEAK_NO_COMMANDS("&4You may not send commands!"),

	SPY_ENABLED("&dSpy mode enabled."),
	SPY_DISABLED("&dSpy mode disabled."),

	STARVE("&dYou have just starved &7{player}&d!"),
	STARVE_NOTICE("&4You have been starved by &7{player}&4!"),
	STARVE_EXEMPT("&4You may not starve that player."),

	SUDO_EXEMPT("&4You may not make that player run commands."),
	SUDO("&dExecuting command &7/{command} &dfrom user &7{player}&d."),

	SUICIDE("&e{player} &4has committed suicide!"),

	TIME_INVALID("&4Invalid time specified!"),
	TIME_CHANGED("&dThe time was changed to &7{ticks} ticks &d(&7{24h}&d/&7{12h}&d) by &7{player}&d."),
	TIME_SET("&dSet time in &7{world} &dto &7{ticks} ticks &d(&7{24h}&d/&7{12h}&d)."),
	TIME_CURRENT("&dThe current time in &7{world} &dis &7{ticks} ticks &d(&7{24h}&d/&7{12h}&d)."),

	TPACCEPT_NONE_PENDING("&4You have no requests pending."),
	TPACCEPT_NOTICE("&dYour teleport request was accepted."),
	TPACCEPT("&dTeleport request accepted."),
	TPDENY_NOTICE("&dYour teleport request was denied."),
	TPDENY("&dTeleport request denied."),
	TPA_NOTICE1("&7{player} &dhas requested to teleport to you."),
	TPA_NOTICE2("&dType &7/tpaccept &dor &7/tpdeny&d."),
	TPA_SENT("&dSent request to &7{player}&d."),
	TPAALL_SENT("&dYou have sent a teleport request to all players."),
	TPAHERE_NOTICE1("&7{player} &dhas requested you to teleport to them."),
	TPAHERE_NOTICE2("&dType &7/tpaccept &dor &7/tpdeny&d."),
	TPAHERE_SENT("&dSent request to &7{player}&d."),
	TPTOGGLE_ENABLED("&dEnabled teleportation."),
	TPTOGGLE_DISABLED("&dDisabled teleportation."),
	TPTOGGLE_OFF("&4That player has teleportation turned off!"),

	UNIVERSE_NO_ONE_HEARS_YOU("&e&oYou are all alone in this universe. Forever alone. :'("),

	WALKSPEED("&dSet your walk speed to &7{speed}&d."),

	WARP_DOES_NOT_EXIST("&4That warp does not exist!"),
	WARP_EXEMPT("&4You cannot warp that player!"),
	WARP_GOING_TO("&dGoing to warp &7{warp}&d."),
	WARP_NO_PERM("&4You do not have permission for that warp!"),
	WARP_OTHER("&dWarping &7{target}&d to &7{warp}&d."),
	WARP_OTHER_NOTICE("&dWarping to warp &7{warp}&d by &7{player}&d."),
	WARP_LIST("&dWarps: {list}"),
	WARP_LIST_EMPTY("&4There are no warps!"),
	WARP_SET("&dWarp &7{warp} &dset."),
	WARP_DELETED("&dWarp &7{warp} &ddeleted."),

	WEATHER_INVALID("&4Invalid condition specified! (sun, rain, storm)"),
	WEATHER_SET("&dSet weather to &7{weather} &din &7{world}&d."),
	WEATHER_SET_DURATION("&dSet weather to &7{weather} &din &7{world} &dfor &7{duration} &dseconds."),

	WORKBENCH("&dOpened a workbench for you.");

	private String def;

	private File configFile;
	private FileConfiguration config;

	private Lang(String def) {
		this.def = def;
		configFile = new File(Main.getInstance().getDataFolder(), Config.LANGUAGE_FILE.getString());
		saveDefault();
		reload();
	}

	public void reload() {
		config = YamlConfiguration.loadConfiguration(configFile);
	}

	public void saveDefault() {
		if (!configFile.exists()) {
			Main.getInstance().saveResource(Config.LANGUAGE_FILE.getString(), false);
		}
	}

	public String get() {
		String key = name().toLowerCase().replace("_", "-");
		String value = config.getString(key, def);
		if (value == null) {
			Logger.log("Missing lang data: " + key);
			value = "&c[missing lang data]";
		}
		return ChatColor.translateAlternateColorCodes('&', value);
	}
}
