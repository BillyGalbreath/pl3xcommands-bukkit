package net.pl3x.bukkit.pl3xcommands.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

public class JailConfig extends YamlConfiguration {
	private static JailConfig config;

	public static JailConfig getConfig() {
		if (config == null) {
			config = new JailConfig();
		}
		return config;
	}

	private File file = null;
	private Object saveLock = new Object();

	public JailConfig() {
		super();
		file = new File(Main.getInstance().getDataFolder(), "jails.yml");
		load();
	}

	private void load() {
		synchronized (saveLock) {
			try {
				this.load(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void forceSave() {
		synchronized (saveLock) {
			try {
				save(file);
			} catch (Exception ignore) {
			}
		}
	}

	public List<String> getJails() {
		return new ArrayList<String>(getConfigurationSection("jails").getValues(false).keySet());
	}

	public Location getJail(String jail) {
		double x = getDouble("jails." + jail + ".x");
		double y = getDouble("jails." + jail + ".y");
		double z = getDouble("jails." + jail + ".z");
		World world = Bukkit.getWorld(getString("jails." + jail + ".world", ""));
		float pitch = (float) getDouble("jails." + jail + ".pitch");
		float yaw = (float) getDouble("jails." + jail + ".yaw");
		if (world == null) {
			return null;
		}
		return new Location(world, x, y, z, yaw, pitch);
	}

	public void setJail(String jail, Location location) {
		set("jails." + jail + ".x", location.getX());
		set("jails." + jail + ".y", location.getY());
		set("jails." + jail + ".z", location.getZ());
		set("jails." + jail + ".world", location.getWorld().getName());
		set("jails." + jail + ".pitch", location.getPitch());
		set("jails." + jail + ".yaw", location.getYaw());
		forceSave();
	}

	public void deleteJail(String jail) {
		set("jails." + jail, null);
		forceSave();
	}
}
