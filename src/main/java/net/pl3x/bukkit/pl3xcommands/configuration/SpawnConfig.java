package net.pl3x.bukkit.pl3xcommands.configuration;

import java.io.File;

import net.pl3x.bukkit.pl3xcommands.Main;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

public class SpawnConfig extends YamlConfiguration {
	private static SpawnConfig config;

	public static SpawnConfig getConfig() {
		if (config == null) {
			config = new SpawnConfig();
		}
		return config;
	}

	private File file = null;
	private Object saveLock = new Object();

	public SpawnConfig() {
		super();
		file = new File(Main.getInstance().getDataFolder(), "spawns.yml");
		load();
	}

	private void load() {
		synchronized (saveLock) {
			try {
				this.load(file);
			} catch (Exception ignore) {
			}
		}
	}

	public void forceSave() {
		synchronized (saveLock) {
			try {
				save(file);
			} catch (Exception ignore) {
			}
		}
	}

	public Location getSpawn(World world) {
		String worldName = world.getName();
		if (get(worldName) == null) {
			return null;
		}
		double x = getDouble(worldName + ".x");
		double y = getDouble(worldName + ".y");
		double z = getDouble(worldName + ".z");
		float pitch = (float) getDouble(worldName + ".pitch");
		float yaw = (float) getDouble(worldName + ".yaw");
		return new Location(world, x, y, z, yaw, pitch);
	}

	public void setSpawn(World world, Location value) {
		String worldName = world.getName();
		set(worldName + ".x", value.getX());
		set(worldName + ".y", value.getY());
		set(worldName + ".z", value.getZ());
		set(worldName + ".pitch", value.getPitch());
		set(worldName + ".yaw", value.getYaw());
		forceSave();
	}
}
