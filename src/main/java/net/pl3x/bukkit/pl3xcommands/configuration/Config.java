package net.pl3x.bukkit.pl3xcommands.configuration;

import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;

public enum Config {
	COLOR_LOGS(true),
	DEBUG_MODE(false),
	LANGUAGE_FILE("lang-en.yml"),
	DISABLED_COMMANDS(null),
	MAIL_CHECK_INTERVAL(300),
	USE_WARP_PERMS(false),
	BACK_ON_DEATH(true),
	EMPTY_BACKPACK_ON_DEATH(false),
	BROADCAST_FORMAT("&b[&aBroadcast&b]&a "),
	MUTE_BLOCKED_COMMANDS(null),
	NICK_PREFIX("|"),
	USE_SMOOTH_TIME_CHANGE(true),
	BROADCAST_TIME_CHANGE(true),
	DEFAULT_EXPLODE_POWER(0),
	MAX_EXPLODE_POWER(4),
	EXPLODE_MAKES_FIRE(true),
	DEFAULT_NEAR_RADIUS(50),
	MAX_NEAR_RADIUS(2000),
	SPAWNMOB_LIMIT(10),
	ALWAYS_DROP_ENTITY_EQUIPMENT_ON_DEATH(true),
	BIOME_MAX_RADIUS(20),
	DEFAULT_CHAT_CHANNEL("LOCAL"),
	LOCAL_CHAT_RANGE(100),
	CHAT_ADMIN_FORMAT("&7[&4Admin&7][&b{prefix}&7] {displayname}&e:&r {message}"),
	CHAT_UNIVERSE_FORMAT("&7[&dUniverse&7][&b{prefix}&7] {displayname}&e:&r {message}"),
	CHAT_GLOBAL_FORMAT("&7[&e{world}&7][&b{prefix}&7] {displayname}&e:&r {message}"),
	CHAT_LOCAL_FORMAT("&7[&b{prefix}&7] {displayname}&e:&r {message}"),
	CHAT_SPY_FORMAT("&7[&6Spy&7][&b{prefix}&7] {displayname}&e:&r {message}"),
	CHAT_TIMESTAMP_FORMAT("hh:mm a");

	private Main plugin;
	private final Object def;

	private Config(Object def) {
		this.plugin = Main.getInstance();
		this.def = def;
	}

	public String getKey() {
		return name().toLowerCase().replace("_", "-");
	}

	public void set(Object value) {
		plugin.getConfig().set(getKey(), value);
	}

	public int getInt() {
		return plugin.getConfig().getInt(getKey(), (Integer) def);
	}

	public double getDouble() {
		return plugin.getConfig().getDouble(getKey(), (Double) def);
	}

	public float getFloat() {
		return (float) plugin.getConfig().getDouble(getKey(), (Double) def);
	}

	public String getString() {
		return plugin.getConfig().getString(getKey(), (String) def);
	}

	public List<String> getStringList() {
		return plugin.getConfig().getStringList(getKey());
	}

	public boolean getBoolean() {
		return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
	}
}
