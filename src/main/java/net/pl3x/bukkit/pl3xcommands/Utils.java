package net.pl3x.bukkit.pl3xcommands;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

public class Utils {
	/**
	 * Join a string list together into a single string
	 * 
	 * @param list A String[] list
	 * @param joiner String joiner
	 * @param start The starting index
	 * @return Single joined string
	 */
	public static String join(String[] list, String joiner, int start) {
		return join(Arrays.asList(list), joiner, start);
	}

	/**
	 * Join a set of strings together into a single string
	 * 
	 * @param set Set of strings
	 * @param joiner String joiner
	 * @param start The starting index
	 * @return Single joined string
	 */
	public static String join(Set<String> list, String joiner, int start) {
		return join(list.toArray(new String[0]), joiner, start);
	}

	/**
	 * Join a list of strings together into a single string
	 * 
	 * @param list List of strings
	 * @param joiner String joiner
	 * @param start The starting index
	 * @return Single joined string
	 */
	public static String join(List<String> list, String joiner, int start) {
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < list.size(); i++) {
			if (i != start) {
				bldr.append(joiner);
			}
			bldr.append(list.get(i));
		}
		return bldr.toString();
	}

	/**
	 * Gets the limit set in a permission node. Max limit is 1024.
	 * 
	 * @param player The player to check permission on
	 * @param node The limit node
	 * @return Integer limit of node, or -1 if unlimited
	 */
	public static Integer getLimit(Player player, String node) {
		int limit = 0;
		int maxlimit = 1024;
		if (player.hasPermission(node + ".*")) {
			return -1;
		}
		for (int i = 0; i < maxlimit; i++) {
			if (player.hasPermission(node + "." + i) && i > limit) {
				limit = i;
			}
		}
		return limit;
	}

	/**
	 * Gets a safe location near the supplied location
	 * 
	 * @param location Supplied location
	 * @return A safe location
	 */
	public static Location getSafeLocation(Location location) {
		return location;
		//double x = location.getX();
		//double y = location.getY();
		//double z = location.getZ();
		//World world = location.getWorld();
		//double max = world.getMaxHeight();
		//if (y <= 0) {
			//y = 64;
		//}
		//double original = y;
		//while (true) {
			//if (y <= 0) {
				//y = max;
			//}
			//Location loc = new Location(world, x, y, z, location.getYaw(), location.getPitch());
			//if (isSafeLocation(loc)) {
				//return loc;
			//}
			//if (y > original && y < original + 1) {
				//return location;
			//}
			//y--;
		//}
	}

	/**
	 * Checks if a location is safe (solid ground with 2 breathable blocks)
	 * 
	 * @param location Location to check
	 * @return True if location is safe
	 */
	public static boolean isSafeLocation(Location location) {
		Block feet = location.getBlock();
		if (!feet.getType().isTransparent() && !feet.getLocation().add(0, 1, 0).getBlock().getType().isTransparent()) {
			return false; // not transparent (will suffocate)
		}
		Block head = feet.getRelative(BlockFace.UP);
		if (!head.getType().isTransparent()) {
			return false; // not transparent (will suffocate)
		}
		Block ground = feet.getRelative(BlockFace.DOWN);
		if (!ground.getType().isSolid()) {
			return false; // not solid
		}
		return true;
	}
}
