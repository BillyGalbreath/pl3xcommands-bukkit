package net.pl3x.bukkit.pl3xcommands;

import java.util.logging.Level;

import net.pl3x.bukkit.pl3xcommands.configuration.Config;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {
	public static void log(String msg) {
		msg = ChatColor.translateAlternateColorCodes('&', "&3[&d" + Main.getInstance().getName() + "&3]&r " + msg);
		if (Config.COLOR_LOGS.getBoolean()) {
			Bukkit.getServer().getConsoleSender().sendMessage(msg);
			return;
		}
		Bukkit.getLogger().log(Level.INFO, ChatColor.stripColor(msg));
	}

	public static void debug(String msg) {
		if (Config.DEBUG_MODE.getBoolean()) {
			Logger.log("&7[&eDEBUG&7]&r " + msg);
		}
	}
}
