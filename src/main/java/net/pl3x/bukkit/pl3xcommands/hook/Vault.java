package net.pl3x.bukkit.pl3xcommands.hook;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Vault {
	private static Chat chat;

	public static Chat getChat() {
		return chat;
	}

	public static boolean setupChat() {
		RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) {
			chat = chatProvider.getProvider();
		}

		return (chat != null);
	}

	public static String getPlayerPrefix(Player player) {
		return chat.getPlayerPrefix(player);
	}

	public static String getPlayerSuffix(Player player) {
		return chat.getPlayerSuffix(player);
	}

	public static String getGroupPrefix(Player player) {
		return chat.getGroupPrefix(player.getWorld(), getPrimaryGroup(player));
	}

	public static String getGroupSuffix(Player player) {
		return chat.getGroupSuffix(player.getWorld(), getPrimaryGroup(player));
	}

	public static String getPrimaryGroup(Player player) {
		return chat.getPrimaryGroup(player);
	}
}
