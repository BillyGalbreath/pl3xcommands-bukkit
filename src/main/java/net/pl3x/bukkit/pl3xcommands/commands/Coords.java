package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Coords implements CommandExecutor {
	private Main plugin;

	public Coords(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "coords");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.coords")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (!(sender instanceof Player) && args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		Location location = player.getLocation();
		if (args.length > 0) {
			if (!sender.hasPermission("others.coords")) {
				plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
				return true;
			}
			Player target = Bukkit.getPlayer(args[0]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			location = target.getLocation();
		}
		plugin.sendMessage(sender, Lang.COORDS.get().replace("{world}", location.getWorld().getName()).replace("{x}", Integer.toString(location.getBlockX())).replace("{y}", Integer.toString(location.getBlockY())).replace("{z}", Integer.toString(location.getBlockZ())));
		return true;
	}
}
