package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.Date;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class Mute implements CommandExecutor {
	private Main plugin;

	public Mute(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "mute")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.mute")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (sender.getName().equals(target.getName())) {
			plugin.sendMessage(sender, Lang.MUTE_YOURSELF.get());
			return true;
		}
		if (target.hasPermission("exempt.mute")) {
			plugin.sendMessage(sender, Lang.MUTE_EXEMPT.get());
			return true;
		}
		PlayerConfig config = PlayerConfig.getConfig(target);
		boolean mute = label.startsWith("un") || label.startsWith("UN");
		if (mute) {
			int muteTime = Integer.MAX_VALUE;
			if (args.length > 1) {
				try {
					muteTime = Integer.valueOf(args[1]);
				} catch (NumberFormatException e) {
					plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
					return true;
				}
			}
			if (muteTime < 0) {
				plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
				return true;
			}
			config.set("mutetime", muteTime);
			config.set("mutedat", System.currentTimeMillis());
			plugin.sendMessage(sender, Lang.MUTE_SET.get().replace("{player}", target.getName()));
			plugin.sendMessage(target, Lang.MUTE_NOTICE.get().replace("{player}", sender.getName()));
		} else {
			config.set("mutetime", null);
			config.set("mutedat", null);
			plugin.sendMessage(sender, Lang.MUTE_UNSET.get().replace("{player}", target.getName()));
			plugin.sendMessage(target, Lang.MUTE_NOTICE_UNMUTE.get().replace("{player}", sender.getName()));
		}
		config.set("muted", mute);
		config.forceSave();
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void muted(AsyncPlayerChatEvent event) {
			Player player = event.getPlayer();
			PlayerConfig config = PlayerConfig.getConfig(player);
			if (!config.getBoolean("muted", false)) {
				return;
			}
			long muteTime = config.getLong("mute-length");
			long mutedAt = config.getLong("muted-at");
			long now = new Date().getTime();
			if (now <= muteTime + mutedAt) {
				config.set("muted", false);
				config.forceSave();
				return;
			}
			String howLong = DurationFormatUtils.formatDurationWords(muteTime + mutedAt - now, true, true);
			plugin.sendMessage(player, Lang.MUTED_CANT_TALK.get().replace("{time}", howLong));
			event.setFormat("");
			event.setCancelled(true);
		}

		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void mute(PlayerCommandPreprocessEvent event) {
			Player player = event.getPlayer();
			PlayerConfig config = PlayerConfig.getConfig(player);
			if (!config.getBoolean("muted", false)) {
				return;
			}
			long muteTime = config.getLong("mute-length");
			long mutedAt = config.getLong("muted-at");
			long now = new Date().getTime();
			if (now <= muteTime + mutedAt) {
				config.set("muted", false);
				config.forceSave();
				return;
			}
			String command = event.getMessage().toLowerCase();
			for (String blockedCommand : Config.MUTE_BLOCKED_COMMANDS.getStringList()) {
				if (!(command.startsWith(blockedCommand.toLowerCase()))) {
					continue;
				}
				String howLong = DurationFormatUtils.formatDurationWords(muteTime + mutedAt - now, true, true);
				plugin.sendMessage(player, Lang.MUTED_CANT_TALK.get().replace("{time}", howLong));
				event.setCancelled(true);
				return;
			}
		}
	}
}
