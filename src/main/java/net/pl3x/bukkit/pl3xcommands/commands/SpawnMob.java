package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public class SpawnMob implements CommandExecutor {
	private Main plugin;

	public SpawnMob(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "spawnmob");
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.spawnmob")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (args.length < 1) {
			List<String> list = new ArrayList<String>();
			for (EntityType entityType : EntityType.values()) {
				if (!entityType.isAlive()) {
					continue;
				}
				if (!entityType.isSpawnable()) {
					continue;
				}
				list.add(entityType.getName().toLowerCase());
			}
			plugin.sendMessage(sender, Lang.SPAWNMOB_LIST.get().replace("{list}", list.toString()));
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		EntityType entityType;
		Location location = player.getTargetBlock(new HashSet<Material>(), 120).getLocation().add(0, 1, 0);
		try {
			entityType = EntityType.valueOf(args[0].toUpperCase());
		} catch (Exception e) {
			plugin.sendMessage(sender, Lang.SPAWNMOB_INVALID.get());
			return true;
		}
		int count = 1;
		if (args.length > 1) {
			try {
				count = Integer.parseInt(args[1]);
			} catch (Exception e) {
				plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
				return true;
			}
			if (count < 0) {
				plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
				return true;
			}
			Integer limit = Config.SPAWNMOB_LIMIT.getInt();
			if (count > limit && !sender.hasPermission("command.spawnmob.nolimit")) {
				count = limit;
			}
		}
		for (int i = 0; i < count; i++) {
			location.getWorld().spawnEntity(location, entityType);
		}
		plugin.sendMessage(sender, Lang.SPAWNMOB.get().replace("{mob}", entityType.getName().toLowerCase()));
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void correctEquipmentCount(CreatureSpawnEvent event) {
			if (!Config.ALWAYS_DROP_ENTITY_EQUIPMENT_ON_DEATH.getBoolean()) {
				return; // let vanilla handle drops
			}

			LivingEntity entity = event.getEntity();

			if (entity instanceof Player) {
				return; // do not modify player equipment counts!
			}

			if (entity instanceof ArmorStand) {
				return; // armor stands are creatures? o_O
			}

			EntityEquipment equipment = entity.getEquipment();

			ItemStack boots = equipment.getBoots();
			ItemStack leggings = equipment.getLeggings();
			ItemStack chestplate = equipment.getChestplate();
			ItemStack helmet = equipment.getHelmet();
			ItemStack hand = equipment.getItemInHand();

			if (boots != null && boots.getAmount() < 1) {
				boots.setAmount(1);
			}
			if (leggings != null && leggings.getAmount() < 1) {
				leggings.setAmount(1);
			}
			if (chestplate != null && chestplate.getAmount() < 1) {
				chestplate.setAmount(1);
			}
			if (helmet != null && helmet.getAmount() < 1) {
				helmet.setAmount(1);
			}
			if (hand != null && hand.getAmount() < 1) {
				hand.setAmount(1);
			}

			equipment.setBoots(boots);
			equipment.setLeggings(leggings);
			equipment.setChestplate(chestplate);
			equipment.setHelmet(helmet);
			equipment.setItemInHand(hand);

			equipment.setBootsDropChance(1F);
			equipment.setLeggingsDropChance(1F);
			equipment.setChestplateDropChance(1F);
			equipment.setHelmetDropChance(1F);
			equipment.setItemInHandDropChance(1F);
		}
	}
}
