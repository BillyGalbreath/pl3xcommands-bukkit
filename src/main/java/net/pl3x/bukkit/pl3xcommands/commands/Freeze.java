package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.PluginManager;

public class Freeze implements CommandExecutor {
	private Main plugin;

	public Freeze(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "freeze")) {
			return;
		}
		PluginManager pluginManager = plugin.getServer().getPluginManager();
		Listener listener = new Listener() {
		};
		EventExecutor executor = new EventExecutor() {
			public void execute(Listener listener, Event event) {
				if (event instanceof PlayerEvent) {
					frozen(event, ((PlayerEvent) event).getPlayer());
				} else if (event instanceof BlockBreakEvent) {
					frozen(event, ((BlockBreakEvent) event).getPlayer());
				} else if (event instanceof BlockPlaceEvent) {
					frozen(event, ((BlockPlaceEvent) event).getPlayer());
				}
			}
		};

		List<Class<? extends Event>> eventsToListenFor = new ArrayList<Class<? extends Event>>();
		eventsToListenFor.add(BlockPlaceEvent.class);
		eventsToListenFor.add(BlockBreakEvent.class);
		eventsToListenFor.add(PlayerTeleportEvent.class);
		eventsToListenFor.add(PlayerCommandPreprocessEvent.class);
		eventsToListenFor.add(PlayerInteractEvent.class);
		eventsToListenFor.add(PlayerInteractEntityEvent.class);
		eventsToListenFor.add(PlayerMoveEvent.class);

		List<EventPriority> eventPrioritiesToListenOn = new ArrayList<EventPriority>();
		eventPrioritiesToListenOn.add(EventPriority.HIGHEST);
		eventPrioritiesToListenOn.add(EventPriority.LOWEST);

		for (Class<? extends Event> iClass : eventsToListenFor) {
			for (EventPriority iPriority : eventPrioritiesToListenOn) {
				pluginManager.registerEvent(iClass, listener, iPriority, executor, plugin, true);
			}
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.freeze")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.freeze")) {
			plugin.sendMessage(sender, Lang.FREEZE_EXEMPT.get());
			return true;
		}
		PlayerConfig config = PlayerConfig.getConfig(target);
		Boolean frozen = !config.getBoolean("frozen");
		config.set("frozen", frozen);
		config.forceSave();
		plugin.sendMessage(sender, Lang.FREEZE.get().replace("{action}", frozen ? "frozen " : "thawed ").replace("{player}", target.getDisplayName()));
		plugin.sendMessage(target, Lang.FREEZE_NOTICE.get().replace("{action}", frozen ? "frozen " : "thawed "));
		return true;
	}

	private void frozen(Event event, Player player) {
		if (!PlayerConfig.getConfig(player).getBoolean("frozen")) {
			return;
		}
		if (event instanceof PlayerMoveEvent) {
			((PlayerMoveEvent) event).setTo(((PlayerMoveEvent) event).getFrom());
			((PlayerMoveEvent) event).setCancelled(true);
			return;
		}
		plugin.sendMessage(player, Lang.FROZEN.get());
		if (event instanceof Cancellable) {
			((Cancellable) event).setCancelled(true);
		}
	}
}
