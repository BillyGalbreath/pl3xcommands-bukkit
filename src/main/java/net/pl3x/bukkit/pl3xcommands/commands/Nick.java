package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Nick implements CommandExecutor {
	private Main plugin;

	public Nick(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "nick")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.nick")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		if (args.length == 1) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, cmd.getDescription());
				return false;
			}
			Player player = (Player) sender;
			String nick = args[0].trim();
			if (nick.equalsIgnoreCase("off") || nick.equalsIgnoreCase("none") || nick.equalsIgnoreCase("clear")) {
				setNick(player, null);
				plugin.sendMessage(sender, Lang.NICK_RESET.get());
				return true;
			}
			nick = setNick(player, nick);
			plugin.sendMessage(sender, Lang.NICK_SET.get().replace("{nick}", nick));
			return true;
		}
		if (!sender.hasPermission("others.nick")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.nick")) {
			plugin.sendMessage(sender, Lang.NICK_EXEMPT.get());
			return true;
		}
		String nick = args[1].trim();
		if (nick.equalsIgnoreCase("off") || nick.equalsIgnoreCase("none") || nick.equalsIgnoreCase("clear")) {
			setNick(target, null);
			plugin.sendMessage(sender, Lang.NICK_RESET_OTHER.get().replace("{player}", target.getName()));
			plugin.sendMessage(target, Lang.NICK_RESET_NOTICE.get());
			return true;
		}
		nick = setNick(target, nick);
		plugin.sendMessage(sender, Lang.NICK_SET_OTHER.get().replace("{player}", target.getName()).replace("{nick}", nick));
		plugin.sendMessage(target, Lang.NICK_SET_NOTICE.get().replace("{nick}", nick));
		return true;
	}

	private String setNick(Player player, String nickname) {
		if (!nickname.equals(null)) {
			nickname = Config.NICK_PREFIX.getString() + nickname;
			if (player.hasPermission("command.nick.color")) {
				nickname = nickname.replaceAll("(?i)&([a-f0-9r])", "\u00a7$1");
			}
			if (player.hasPermission("command.nick.style")) {
				nickname = nickname.replaceAll("(?i)&([m-or])", "\u00a7$1");
			}
			if (player.hasPermission("command.nick.magic")) {
				nickname = nickname.replaceAll("(?i)&([kr])", "\u00a7$1");
			}
		}
		PlayerConfig config = PlayerConfig.getConfig(player);
		config.set("nickname", nickname);
		config.forceSave();
		player.setDisplayName(nickname);
		player.setPlayerListName(player.getName());
		return nickname;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void setNickName(PlayerJoinEvent event) {
			Player player = event.getPlayer();
			String nickname = PlayerConfig.getConfig(player).getString("nickname");
			if (nickname == null || nickname.equals("")) {
				nickname = null;
			}
			player.setDisplayName(nickname);
			player.setPlayerListName(player.getName());
		}
	}
}
