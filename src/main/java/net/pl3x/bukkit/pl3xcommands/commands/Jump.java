package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashSet;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Jump implements CommandExecutor {
	private Main plugin;

	public Jump(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "jump");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.jump")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		HashSet<Material> transparent = new HashSet<Material>();
		transparent.add(Material.AIR);
		Block block = player.getTargetBlock(transparent, 120);
		if (block == null || block.getType().equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.JUMP_NOT_THERE.get());
			return true;
		}
		Location playerLoc = player.getLocation();
		Location location = block.getLocation().add(0.5, 1, 0.5);
		location.setPitch(playerLoc.getPitch());
		location.setYaw(playerLoc.getYaw());
		player.teleport(location);
		return true;
	}
}
