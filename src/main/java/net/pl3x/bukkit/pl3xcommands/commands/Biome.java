package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Biome implements CommandExecutor {
	private Main plugin;

	public Biome(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "biome");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.biome")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (args.length < 1) {
			String biome = player.getLocation().getBlock().getBiome().name().toLowerCase();
			plugin.sendMessage(sender, Lang.BIOME_CURRENT.get().replace("{biome}", biome));
			return true;
		}
		if (args[0].equalsIgnoreCase("list")) {
			StringBuilder sb = new StringBuilder();
			for (org.bukkit.block.Biome biome : org.bukkit.block.Biome.values()) {
				sb.append(biome.toString().toLowerCase().replace("_", "-")).append(", ");
			}
			String biomes = sb.toString();
			plugin.sendMessage(sender, Lang.BIOME_LIST.get().replace("{list}", biomes.substring(0, biomes.length() - 2)));
			return true;
		}
		if (args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		org.bukkit.block.Biome biome = null;
		try {
			biome = org.bukkit.block.Biome.valueOf(args[0].toUpperCase().replace("-", "_"));
		} catch (Exception ignored) {
		}
		if (biome == null) {
			plugin.sendMessage(sender, Lang.BIOME_NOT_FOUND.get());
			return true;
		}
		int radius = 0;
		try {
			radius = Integer.valueOf(args[1]);
		} catch (NumberFormatException e) {
			plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
			return true;
		}
		if (radius <= 0) {
			plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
			return true;
		}
		if (radius > Config.BIOME_MAX_RADIUS.getInt()) {
			plugin.sendMessage(sender, Lang.ERROR_NUNBER_TOO_BIG.get());
			return true;
		}
		int radiusSquared = radius * radius;
		Location location = player.getLocation();
		World world = location.getWorld();
		int sx = location.getBlockX();
		int sz = location.getBlockZ();
		for (int x = -radius; x <= radius; x++) {
			for (int z = -radius; z <= radius; z++) {
				if (x * x + z * z <= radiusSquared) {
					world.getBlockAt(sx + x, 0, sz + z).setBiome(biome);
					world.refreshChunk((sx + x) >> 4, (sz + z) >> 4);
				}
			}
		}
		plugin.sendMessage(sender, Lang.BIOME_SET.get());
		return true;
	}
}
