package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.Map;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class ListHome implements CommandExecutor {
	private Main plugin;

	public ListHome(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "listhome");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.listhome")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (!(sender instanceof Player) && args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target;
		if (args.length < 1)
			target = (Player) sender;
		else {
			if (!sender.hasPermission("others.listhome")) {
				plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
				return true;
			}
			target = Bukkit.getPlayer(args[0]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			if (target.hasPermission("exempt.listhome")) {
				plugin.sendMessage(sender, Lang.HOME_LIST_EXEMPT.get());
				return true;
			}
		}
		ConfigurationSection cfgs = PlayerConfig.getConfig(target).getConfigurationSection("home");
		if (cfgs == null) {
			plugin.sendMessage(sender, Lang.HOME_FOUND_NONE.get());
			return true;
		}
		Map<String, Object> opts = cfgs.getValues(false);
		if (opts.keySet().isEmpty()) {
			plugin.sendMessage(sender, Lang.HOME_FOUND_NONE.get());
			return true;
		}
		String homes = opts.keySet().toString();
		homes = homes.substring(1, homes.length() - 1);
		plugin.sendMessage(sender, Lang.HOME_LIST.get());
		plugin.sendMessage(sender, homes);
		return true;
	}
}
