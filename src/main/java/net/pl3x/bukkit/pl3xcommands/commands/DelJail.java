package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.JailConfig;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelJail implements CommandExecutor {
	private Main plugin;

	public DelJail(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "deljail");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.deljail")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		String jail = args[0].toLowerCase();
		JailConfig config = JailConfig.getConfig();
		Location loc = config.getJail(jail);
		if (loc == null) {
			plugin.sendMessage(sender, Lang.JAIL_NOT_FOUND.get());
			return true;
		}
		config.deleteJail(jail);
		plugin.sendMessage(sender, Lang.JAIL_DELETED.get().replace("{jail}", jail));
		return true;
	}
}
