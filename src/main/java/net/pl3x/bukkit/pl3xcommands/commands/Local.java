package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.Set;

import net.pl3x.bukkit.pl3xcommands.ChatManager;
import net.pl3x.bukkit.pl3xcommands.ChatManager.Channel;
import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Local implements CommandExecutor {
	private Main plugin;

	public Local(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "local");
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.local")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		ChatManager.getManager().setChannel(player, Channel.LOCAL);
		player.chat(Utils.join(args, " ", 0));
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onChat(AsyncPlayerChatEvent event) {
			Player player = event.getPlayer();
			ChatManager chatManager = ChatManager.getManager();
			String original = chatManager.sanitizeInput(event.getMessage());
			String message = chatManager.replaceVariables(player, original);
			if (player.hasPermission("command.local.color")) {
				message = ChatColor.translateAlternateColorCodes('&', message);
			}
			event.setFormat(message);
			event.getRecipients().clear();
			Set<Player> recipients = chatManager.getRecipients(player);
			for (Player recipient : recipients) {
				event.getRecipients().add(recipient);
			}
			String spyMsg = chatManager.getSpyMessage(player, original);
			for (Player spy : chatManager.getSpies(recipients)) {
				plugin.sendMessage(spy, spyMsg);
			}
		}
	}
}
