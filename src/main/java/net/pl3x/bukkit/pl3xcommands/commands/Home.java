package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class Home implements TabExecutor {
	private Main plugin;

	public Home(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "home");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return null;
		}
		if (args.length == 1) {
			return getMatchingHomeNames((Player) sender, args[0]);
		}
		return null;
	}

	public static List<String> getMatchingHomeNames(Player player, String name) {
		List<String> list = new ArrayList<String>();
		ConfigurationSection cfg = PlayerConfig.getConfig(player).getConfigurationSection("home");
		if (cfg == null) {
			return null;
		}
		for (String home : cfg.getValues(false).keySet()) {
			if (home.toLowerCase().startsWith(name.toLowerCase())) {
				list.add(home);
			}
		}
		return list;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.home")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		String home = (args.length > 0) ? args[0] : "home";
		PlayerConfig config;
		if (home.contains(":") && sender.hasPermission("others.home")) {
			String[] str = home.split(":");
			if (str.length < 2) {
				plugin.sendMessage(sender, Lang.HOME_OTHER_ERROR.get());
				return true;
			}
			Player target = Bukkit.getPlayer(str[0]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			if (target.hasPermission("exempt.home")) {
				plugin.sendMessage(sender, Lang.HOME_NO_OTHER.get());
				return true;
			}
			config = PlayerConfig.getConfig(target);
			home = str[1];
		} else {
			config = PlayerConfig.getConfig(player);
		}
		Location loc = config.getLocation("home." + home);
		if (loc == null) {
			plugin.sendMessage(sender, Lang.HOME_DOES_NOT_EXIST.get());
			return true;
		}
		if (args.length > 0) {
			plugin.sendMessage(sender, Lang.HOME.get().replace("{home}", home));
		} else {
			plugin.sendMessage(sender, Lang.HOME_DEFAULT.get());
		}
		player.teleport(loc);
		return true;
	}
}
