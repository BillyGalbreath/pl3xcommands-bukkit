package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RageQuit implements CommandExecutor {
	private Main plugin;

	public RageQuit(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "ragequit");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.quit")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			if (sender instanceof Player) {
				plugin.sendBroadcast(Lang.RAGEQUIT_BROADCAST.get().replace("{player}", sender.getName()));
				((Player) sender).kickPlayer(Lang.RAGEQUIT.get() + "\00-silent");
				return true;
			}
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		if (!sender.hasPermission("others.ragequit")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.ragequit")) {
			plugin.sendMessage(sender, Lang.RAGEQUIT_EXEMPT.get());
			return true;
		}
		plugin.sendBroadcast(Lang.RAGEQUIT_BROADCAST.get().replace("{player}", target.getName()));
		target.kickPlayer(Lang.RAGEQUIT.get() + "\00-silent");
		return true;
	}
}
