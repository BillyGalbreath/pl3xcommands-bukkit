package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.Date;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Seen implements CommandExecutor {
	private Main plugin;

	public Seen(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "seen")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.seen")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
		if (target.isOnline()) {
			plugin.sendMessage(sender, Lang.SEEN_NOW.get().replace("{player}", target.getName()));
			return true;
		}
		PlayerConfig config = PlayerConfig.getConfig(target);
		if (config == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (config.get("seen") == null) {
			plugin.sendMessage(sender, Lang.SEEN_UNKNOWN.get());
			return true;
		}
		long seen = config.getLong("seen");
		if (seen < 1L) {
			plugin.sendMessage(sender, Lang.SEEN_UNKNOWN.get());
			return true;
		}
		String lastseen = DurationFormatUtils.formatDurationWords(new Date().getTime() - seen, true, true);
		plugin.sendMessage(sender, Lang.SEEN.get().replace("{player}", target.getName()).replace("{time}", lastseen));
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void lastSeen(PlayerQuitEvent event) {
			PlayerConfig config = PlayerConfig.getConfig(event.getPlayer());
			config.set("seen", new Date().getTime());
			config.forceSave();
		}
	}
}
