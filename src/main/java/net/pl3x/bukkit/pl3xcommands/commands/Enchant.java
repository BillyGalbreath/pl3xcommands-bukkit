package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Enchant implements CommandExecutor {
	private Main plugin;

	public Enchant(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "enchant");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.enchant")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;

		// get item to enchant
		ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getType().equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.ERROR_NOTHING_IN_HAND.get());
			return true;
		}

		// get level to enchant to
		int level;
		if (args.length < 2) {
			level = -1;
		} else if (args[1].equalsIgnoreCase("max")) {
			level = -2;
		} else {
			try {
				level = (int) Short.parseShort(args[1]);
			} catch (NumberFormatException e) {
				plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
				return true;
			}
			if (level < 0) {
				plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
				return true;
			}
			if (level > Short.MAX_VALUE) {
				plugin.sendMessage(sender, Lang.ERROR_NUNBER_TOO_BIG.get());
				return true;
			}
		}

		// remove all enchants from item
		if (level == 0) {
			for (Enchantment enchantment : Enchantment.values()) {
				if (!hand.containsEnchantment(enchantment)) {
					continue;
				}
				hand.removeEnchantment(enchantment);
			}
			plugin.sendMessage(sender, Lang.ENCHANT_REMOVED.get().replace("{item}", getName(hand)));
			return true;
		}
		if (level == -2 && !player.hasPermission("enchant.bypass.level")) {
			plugin.sendMessage(sender, Lang.ENCHANT_LEVEL_TOO_HIGH.get());
			return true;
		}

		// enchant the item
		Enchantment enchantment = Enchantment.getByName(args[0].toUpperCase());
		if (enchantment != null) {
			applyEnchantment(player, hand, enchantment, level);
			plugin.sendMessage(sender, Lang.ENCHANT_ADDED.get().replace("{enchantment}", getName(enchantment)).replace("{item}", getName(hand)).replace("{level}", Integer.toString(level)));
			return true;
		}

		// add all enchants to the item
		if (args[0].equalsIgnoreCase("all")) {
			for (Enchantment allEnchants : Enchantment.values()) {
				applyEnchantment(player, hand, allEnchants, level);
			}
			plugin.sendMessage(sender, Lang.ENCHANT_ADDED_ALL.get().replace("{item}", getName(hand)).replace("{level}", Integer.toString(level)));
			return true;
		}

		// show list of available enchants
		StringBuilder list = new StringBuilder();
		for (Enchantment listEnchants : Enchantment.values()) {
			list.append(listEnchants.getName());
		}
		plugin.sendMessage(sender, Lang.ENCHANT_NOT_FOUND.get());
		plugin.sendMessage(sender, Lang.ENCHANT_LIST.get().replace("{list}", list.toString()));
		return true;
	}

	private void applyEnchantment(Player player, ItemStack item, Enchantment enchantment, int level) {
		switch (level) {
			case -1: // Max level
				level = enchantment.getMaxLevel();
			case -2: // Max short value
				level = Short.MAX_VALUE;
		}
		if (level > enchantment.getMaxLevel() && !player.hasPermission("enchant.bypass.level")) {
			plugin.sendMessage(player, Lang.ENCHANT_LEVEL_TOO_HIGH.get());
			return;
		}
		if (!enchantment.canEnchantItem(item) && !player.hasPermission("enchat.bypass.illegal")) {
			plugin.sendMessage(player, Lang.ENCHANT_ILLEGAL_ITEM.get());
			return;
		}
		if (!player.hasPermission("enchant.bypass.illegal")) {
			for (Enchantment conflict : item.getEnchantments().keySet()) {
				if (enchantment.conflictsWith(conflict)) {
					plugin.sendMessage(player, Lang.ENCHANT_CONFLICT.get().replace("{enchantment}", getName(enchantment)).replace("{conflict}", getName(conflict)));
					return;
				}
			}
		}
		item.addUnsafeEnchantment(enchantment, level);
	}

	private String getName(Enchantment enchantment) {
		return enchantment.getName().toLowerCase().replace("_", " ");
	}

	private String getName(ItemStack item) {
		return item.getType().name().toLowerCase().replace("_", " ");
	}
}
