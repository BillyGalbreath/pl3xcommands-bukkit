package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GameMode implements CommandExecutor {
	private Main plugin;

	public GameMode(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "gamemode");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.gamemode")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		GM gameMode = null;
		for (GM gm : GM.values()) {
			if (gm.name().toLowerCase().startsWith(args[0])) {
				gameMode = gm;
				break;
			}
		}
		if (gameMode == null) {
			plugin.sendMessage(sender, Lang.GAMEMODE_UNKNOWN.get());
			return true;
		}
		Player target = player;
		if (args.length == 2) {
			target = Bukkit.getPlayer(args[1]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			if (!target.getUniqueId().equals(player.getUniqueId()) && !player.hasPermission("others.gamemode")) {
				plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
				return true;
			}
			if (!target.getUniqueId().equals(player.getUniqueId()) && target.hasPermission("exempt.gamemode")) {
				plugin.sendMessage(sender, Lang.GAMEMODE_EXEMPT.get());
				return true;
			}
		} else {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return false;
			}
		}
		target.setGameMode(gameMode.get());
		if (!target.getUniqueId().equals(player.getUniqueId())) {
			plugin.sendMessage(sender, Lang.GAMEMODE_SET_OTHER.get().replace("{gamemode}", gameMode.name().toLowerCase()).replace("{player}", target.getName()));
			plugin.sendMessage(target, Lang.GAMEMODE_SET.get().replace("{gamemode}", gameMode.name().toLowerCase()));
		} else {
			plugin.sendMessage(sender, Lang.GAMEMODE_SET.get().replace("{gamemode}", gameMode.name().toLowerCase()));
		}
		return true;
	}

	private enum GM {
		ADVENTURE,
		CREATIVE,
		SURVIVAL,
		SPECTATOR;

		public org.bukkit.GameMode get() {
			return org.bukkit.GameMode.valueOf(name());
		}
	}
}
