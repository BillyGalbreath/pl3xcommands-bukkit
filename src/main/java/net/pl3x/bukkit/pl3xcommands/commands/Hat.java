package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Hat implements CommandExecutor {
	private Main plugin;

	public Hat(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "hat");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.hat")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		ItemStack hat = player.getItemInHand();
		ItemStack oldHat = player.getInventory().getHelmet();
		if (hat == null || hat.getType().equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.ERROR_NOTHING_IN_HAND.get());
			return true;
		}
		if (hat.getAmount() == 1) {
			player.getInventory().remove(hat);
			if (oldHat != null && !oldHat.getType().equals(Material.AIR)) {
				player.getInventory().addItem(oldHat);
			}
			player.getInventory().setHelmet(hat);
		} else {
			if (oldHat != null && !oldHat.getType().equals(Material.AIR)) {
				if (player.getInventory().firstEmpty() == -1) {
					plugin.sendMessage(sender, Lang.HAT_FULL_INVENTORY.get());
					return true;
				}
				player.getInventory().addItem(oldHat);
			}
			hat.setAmount(hat.getAmount() - 1);
			player.setItemInHand(hat);
			hat.setAmount(1);
			player.getInventory().setHelmet(hat);
		}
		plugin.sendMessage(sender, Lang.HAT.get());
		return true;
	}
}
