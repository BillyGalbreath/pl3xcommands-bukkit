package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

public class FlySpeed implements CommandExecutor {
	private Main plugin;

	public FlySpeed(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "flyspeed")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.flyspeed")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		int flySpeed;
		try {
			flySpeed = Integer.valueOf(args[0]);
		} catch (NumberFormatException e) {
			plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
			return true;
		}
		if (flySpeed < 0 || flySpeed > 10) {
			plugin.sendMessage(sender, Lang.ERROR_NUMBER_NOT_BETWEEN.get().replace("{number1}", "0").replace("{number2}", "10"));
			return true;
		}
		Player target;
		if (args.length < 2) {
			target = (Player) sender;
		} else {
			target = Bukkit.getPlayer(args[1]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			if (target.hasPermission("exempt.flyspeed")) {
				plugin.sendMessage(sender, Lang.FLYSPEED_EXEMPT.get());
				return true;
			}
		}
		plugin.sendMessage(sender, Lang.FLYSPEED.get().replace("{speed}", Integer.toString(flySpeed)));
		target.setFlySpeed(flySpeed / 10F);
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
			Player player = event.getPlayer();
			player.setFlySpeed(0.1F);
		}
	}
}
