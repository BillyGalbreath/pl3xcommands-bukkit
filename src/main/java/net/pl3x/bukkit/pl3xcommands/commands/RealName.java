package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RealName implements CommandExecutor {
	private Main plugin;

	public RealName(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "realname");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.realname")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = null;
		for (Player player : Bukkit.getOnlinePlayers())
			if (player.getDisplayName().equalsIgnoreCase(args[0])) {
				target = player;
			}
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		plugin.sendMessage(sender, Lang.REALNAME.get().replace("{nickname}", target.getDisplayName()).replace("{realname}", target.getName()));
		return true;
	}
}
