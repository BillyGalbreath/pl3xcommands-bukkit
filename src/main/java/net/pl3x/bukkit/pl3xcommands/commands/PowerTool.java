package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PowerTool implements CommandExecutor {
	private Main plugin;

	public PowerTool(Main plugin) {
		this.plugin = plugin;
		if (plugin.registerCommand(this, "powertool")) {
			Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.powertool")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		ItemStack item = player.getItemInHand();
		Material mat = item.getType();
		if (item == null || mat.equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.ERROR_NOTHING_IN_HAND.get());
			return true;
		}
		PlayerConfig config = PlayerConfig.getConfig(player);
		if (args.length == 0) {
			config.set("powertool." + mat.toString(), null);
			config.forceSave();
			plugin.sendMessage(sender, Lang.POWERTOOL_REMOVED.get());
			return true;
		}
		List<String> cmds = config.getStringList("powertool." + mat.toString());
		if (cmds == null) {
			cmds = new ArrayList<String>();
		}
		cmds.add(Utils.join(args, " ", 0));
		config.set("powertool." + mat.toString(), cmds);
		config.forceSave();
		plugin.sendMessage(sender, Lang.POWERTOOL_ADDED.get());
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.LOWEST)
		public void powertool(PlayerInteractEvent event) {
			if (event.getAction().equals(Action.PHYSICAL)) {
				return;
			}
			ItemStack hand = event.getItem();
			if (hand == null || hand.getType().equals(Material.AIR)) {
				return;
			}
			Player player = event.getPlayer();
			List<String> commands = PlayerConfig.getConfig(player).getStringList("powertool." + hand.getType().toString());
			if (commands == null || commands.isEmpty()) {
				return;
			}
			for (String command : commands) {
				if (command.toLowerCase().trim().startsWith("c:")) {
					player.chat(command.trim().substring(2));
				} else {
					player.performCommand(command.trim());
				}
				event.setCancelled(true);
			}
		}

		@EventHandler(priority = EventPriority.LOWEST)
		public void powertool(PlayerInteractEntityEvent event) {
			Player player = event.getPlayer();
			ItemStack hand = player.getItemInHand();
			if (hand == null || hand.getType().equals(Material.AIR)) {
				return;
			}
			List<String> commands = PlayerConfig.getConfig(player).getStringList("powertool." + hand.getType().toString());
			if (commands == null || commands.isEmpty()) {
				return;
			}
			Player clicked = null;
			if (event.getRightClicked() instanceof Player) {
				clicked = (Player) event.getRightClicked();
			}
			for (String command : commands) {
				if (clicked != null) {
					command = command.replace("{player}", clicked.getDisplayName());
				}
				if (command.toLowerCase().trim().startsWith("c:")) {
					player.chat(command.trim().substring(2));
				} else {
					player.performCommand(command.trim());
				}
			}
		}

		@EventHandler(priority = EventPriority.LOWEST)
		public void powertool(EntityDamageByEntityEvent event) {
			Entity entity = event.getDamager();
			if (!(entity instanceof Player)) {
				return;
			}
			powertool(new PlayerInteractEntityEvent((Player) entity, event.getEntity()));
		}
	}
}
