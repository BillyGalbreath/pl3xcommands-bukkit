package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Explosive;
import org.bukkit.entity.LargeFireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.entity.WitherSkull;
import org.bukkit.util.Vector;

public class Fireball implements CommandExecutor {
	private Main plugin;

	public Fireball(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "fireball");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.fireball")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Class<? extends Entity> type = org.bukkit.entity.Fireball.class;
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("small")) {
				type = SmallFireball.class;
			} else if (args[0].equalsIgnoreCase("arrow")) {
				type = Arrow.class;
			} else if (args[0].equalsIgnoreCase("skull")) {
				type = WitherSkull.class;
			} else if (args[0].equalsIgnoreCase("egg")) {
				type = Egg.class;
			} else if (args[0].equalsIgnoreCase("snowball")) {
				type = Snowball.class;
			} else if (args[0].equalsIgnoreCase("expbottle")) {
				type = ThrownExpBottle.class;
			} else if (args[0].equalsIgnoreCase("large")) {
				type = LargeFireball.class;
			}

		}
		Player player = (Player) sender;
		Vector direction = player.getEyeLocation().getDirection().multiply(2);
		Projectile projectile = (Projectile) player.getWorld().spawn(player.getEyeLocation().add(direction.getX(), direction.getY(), direction.getZ()), type);
		projectile.setShooter(player);
		projectile.setVelocity(direction);
		if (projectile instanceof Explosive) {
			((Explosive) projectile).setIsIncendiary(true);
		}
		return true;
	}
}
