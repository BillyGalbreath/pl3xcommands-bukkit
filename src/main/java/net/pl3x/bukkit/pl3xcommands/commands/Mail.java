package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Mail implements CommandExecutor {
	private Main plugin;

	public Mail(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "mail")) {
			return;
		}
		new MailChecker().runTaskTimer(plugin, 20L, Config.MAIL_CHECK_INTERVAL.getInt() * 20L);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.mail")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		if (args[0].equalsIgnoreCase("read")) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return false;
			}
			List<String> mails = PlayerConfig.getConfig((Player) sender).getStringList("mail");
			if (mails.isEmpty()) {
				plugin.sendMessage(sender, Lang.MAIL_NONE.get());
				return true;
			}
			for (String mail : mails) {
				String[] splitMail = mail.split(": ", 2);
				if (splitMail.length < 2) {
					continue;
				}
				String from = splitMail[0];
				String message = splitMail[1];
				plugin.sendMessage(sender, Lang.MAIL_ENTRY.get().replace("{from}", from).replace("{message}", message));
			}
			return true;
		} else if (args[0].equalsIgnoreCase("clear")) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return false;
			}
			PlayerConfig config = PlayerConfig.getConfig((Player) sender);
			config.set("mail", null);
			config.forceSave();
			plugin.sendMessage(sender, Lang.MAIL_CLEARED.get());
			return true;
		} else if (args[0].equalsIgnoreCase("send")) {
			if (!sender.hasPermission("command.mail.send")) {
				plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
				return true;
			}
			if (args.length < 3) {
				plugin.sendMessage(sender, cmd.getDescription());
				return false;
			}
			OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
			if (!target.hasPlayedBefore()) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return true;
			}
			String newmail = sender.getName() + ": " + Utils.join(args, " ", 2);
			PlayerConfig config = PlayerConfig.getConfig(target);
			List<String> mail = config.getStringList("mail");
			mail.add(newmail);
			config.set("mail", mail);
			config.forceSave();
			plugin.sendMessage(sender, Lang.MAIL_SENT.get());
			return true;
		}
		plugin.sendMessage(sender, cmd.getDescription());
		return false;
	}

	public class MailChecker extends BukkitRunnable {
		@Override
		public void run() {
			for (Player player : Bukkit.getOnlinePlayers()) {
				checkMail(player);
			}
		}

		private void checkMail(Player player) {
			if (!player.hasPermission("command.mail")) {
				return;
			}
			List<String> mail = PlayerConfig.getConfig(player).getStringList("mail");
			if (!mail.isEmpty()) {
				plugin.sendMessage(player, Lang.MAIL_NOTIFICATION.get().replace("{count}", Integer.toString(mail.size())));
			}
		}
	}
}
