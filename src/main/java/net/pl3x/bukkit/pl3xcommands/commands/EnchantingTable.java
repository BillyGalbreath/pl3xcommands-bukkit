package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EnchantingTable implements CommandExecutor {
	private Main plugin;

	public EnchantingTable(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "enchantingtable");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.enchantingtable")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		((Player) sender).openEnchanting(null, true);
		plugin.sendMessage(sender, Lang.ENCHANTING_TABLE_OPENED.get());
		return true;

	}
}
