package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.SpawnConfig;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn implements CommandExecutor {
	private Main plugin;

	public SetSpawn(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "setspawn");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.setspawn")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		Location loc = player.getLocation();
		World world = loc.getWorld();

		SpawnConfig.getConfig().setSpawn(world, loc);
		world.setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());

		plugin.sendMessage(sender, Lang.SPAWN_SET.get().replace("{world}", world.getName()));
		return true;
	}
}
