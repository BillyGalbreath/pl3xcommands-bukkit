package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

public class WalkSpeed implements CommandExecutor {
	private Main plugin;

	public WalkSpeed(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "walkspeed")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.walkspeed")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		float speed;
		try {
			speed = Float.valueOf(args[0]);
		} catch (NumberFormatException e) {
			plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
			return true;
		}
		speed /= 10;
		if (speed < 0F || speed > 1F) {
			plugin.sendMessage(sender, Lang.ERROR_NUMBER_NOT_BETWEEN.get().replace("{number1}", "0").replace("{number2}", "10"));
			return true;
		}
		plugin.sendMessage(sender, Lang.WALKSPEED.get().replace("{speed}", Float.toString(speed)));
		player.setWalkSpeed(speed);
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
			Player player = event.getPlayer();
			player.setWalkSpeed(0.2F);
		}
	}
}
