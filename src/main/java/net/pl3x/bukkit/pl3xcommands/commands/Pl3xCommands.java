package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Pl3xCommands implements CommandExecutor {
	private Main plugin;

	public Pl3xCommands(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "pl3xcommands");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.pl3xcommands")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reload();
				plugin.sendMessage(sender, Lang.PL3XCOMMANDS_RELOAD.get().replace("{name}", plugin.getName()));
			}
		}
		plugin.sendMessage(sender, Lang.PL3XCOMMANDS_VERSION.get().replace("{name}", plugin.getName()).replace("{version}", plugin.getDescription().getVersion()));
		return true;
	}
}
