package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.EnumMap;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;

public class Suicide implements CommandExecutor {
	private Main plugin;

	public Suicide(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "suicide");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.suicide")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		player.setLastDamageCause(new EntityDamageByEntityEvent(player, player, EntityDamageEvent.DamageCause.SUICIDE, new EnumMap<DamageModifier, Double>(ImmutableMap.of(DamageModifier.BASE, 0D)), new EnumMap<DamageModifier, Function<? super Double, Double>>(ImmutableMap.of(DamageModifier.BASE, Functions.constant(-0.0)))));
		player.setHealth(0);
		Bukkit.broadcastMessage(Lang.SUICIDE.get().replace("{player}", player.getDisplayName()));
		return true;
	}
}
