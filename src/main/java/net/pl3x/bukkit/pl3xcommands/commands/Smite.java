package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashSet;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Smite implements CommandExecutor {
	private Main plugin;

	public Smite(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "smite");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.smite")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (!(sender instanceof Player) && args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		boolean mega = label.toLowerCase().startsWith("mega") ? true : false;
		if (args.length < 1) {
			Player player = (Player) sender;
			HashSet<Material> transparent = new HashSet<Material>();
			transparent.add(Material.AIR);
			Block block = player.getTargetBlock(transparent, 120);
			if (block == null) {
				plugin.sendMessage(sender, Lang.SMITE_NOT_THERE.get());
				return true;
			}
			for (int i = 0; i < (mega ? 15 : 1); i++) {
				player.getWorld().strikeLightning(block.getLocation());
			}
			return true;
		}
		if (!sender.hasPermission("others.smite")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.smite")) {
			plugin.sendMessage(sender, Lang.SMITE_EXEMPT.get());
			return true;
		}
		plugin.sendMessage(sender, Lang.SMITE.get().replace("{player}", target.getName()));
		plugin.sendMessage(target, Lang.SMITE_VICTIM.get().replace("{player}", sender.getName()));
		for (int i = 0; i < (mega ? 15 : 1); i++) {
			target.getWorld().strikeLightning(target.getLocation());
		}
		return true;
	}
}
