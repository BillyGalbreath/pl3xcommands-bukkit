package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.PluginManager;

public class Buddha implements CommandExecutor {
	private Main plugin;

	public Buddha(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "buddha")) {
			return;
		}
		PluginManager pluginManager = plugin.getServer().getPluginManager();
		Listener listener = new Listener() {
		};
		EventExecutor executor = new EventExecutor() {
			public void execute(Listener listener, Event event) {
				if (event instanceof EntityDamageEvent) {
					buddhaMode((EntityDamageEvent) event);
				}
			}
		};

		List<Class<? extends Event>> eventsToListenFor = new ArrayList<Class<? extends Event>>();
		eventsToListenFor.add(EntityDamageEvent.class);

		List<EventPriority> eventPrioritiesToListenOn = new ArrayList<EventPriority>();
		eventPrioritiesToListenOn.add(EventPriority.HIGHEST);
		eventPrioritiesToListenOn.add(EventPriority.LOWEST);

		for (Class<? extends Event> iClass : eventsToListenFor) {
			for (EventPriority iPriority : eventPrioritiesToListenOn) {
				pluginManager.registerEvent(iClass, listener, iPriority, executor, plugin, true);
			}
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.buddha")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length > 0) {
			if (!sender.hasPermission("others.buddha")) {
				plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
				return true;
			}
			Player target = Bukkit.getPlayer(args[0]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			PlayerConfig config = PlayerConfig.getConfig(target);
			if (!config.getBoolean("buddha")) {
				config.set("buddha", true);
				plugin.sendMessage(target, Lang.BUDDHA_ENABLED_BY.get().replace("{player}", sender.getName()));
				plugin.sendMessage(sender, Lang.BUDDHA_ENABLED_FOR.get().replace("{player}", target.getName()));
			} else {
				config.set("buddha", false);
				plugin.sendMessage(target, Lang.BUDDHA_DISABLED_BY.get().replace("{player}", sender.getName()));
				plugin.sendMessage(sender, Lang.BUDDHA_DISABLED_FOR.get().replace("{player}", target.getName()));
			}
			config.forceSave();
			return true;
		}
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		Player player = (Player) sender;
		PlayerConfig config = PlayerConfig.getConfig(player);
		if (!config.getBoolean("buddha")) {
			config.set("buddha", true);
			plugin.sendMessage(sender, Lang.BUDDHA_ENABLED.get());
		} else {
			config.set("buddha", false);
			plugin.sendMessage(sender, Lang.BUDDHA_DISABLED.get());
		}
		config.forceSave();
		return true;
	}

	private void buddhaMode(EntityDamageEvent event) {
		Entity entity = event.getEntity();
		if (!(entity instanceof Player)) {
			return;
		}
		Player player = (Player) entity;
		if (!PlayerConfig.getConfig(player).getBoolean("buddha")) {
			return;
		}
		if (player.getHealth() == 1) {
			event.setCancelled(true);
		}
		if (event.getDamage() >= player.getHealth()) {
			event.setDamage(player.getHealth() - 1);
		}
		if (player.getHealth() - event.getDamage() <= 1) {
			player.setHealth(1);
			event.setCancelled(true);
		}
	}
}
