package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Facepalm implements CommandExecutor {
	private Main plugin;

	public Facepalm(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "facepalm");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.facepalm")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			Bukkit.broadcastMessage(Lang.FACEPALM.get().replace("{player}", sender.getName()));
			return true;
		}
		Player victim = Bukkit.getPlayer(args[0]);
		if (victim == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (victim.hasPermission("exempt.facepalm")) {
			plugin.sendMessage(sender, Lang.FACEPALM_EXEMPT.get());
			return true;
		}
		Bukkit.broadcastMessage(Lang.FACEPALM_AT.get().replace("{player}", sender.getName()).replace("{target}", victim.getName()));
		return true;
	}
}
