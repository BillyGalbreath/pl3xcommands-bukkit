package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Fart implements CommandExecutor {
	private Main plugin;

	public Fart(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "fart");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.fart")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		Location location = player.getLocation();
		location.getWorld().createExplosion(location, 0F);
		Cow cow = (Cow) location.getWorld().spawnEntity(location, EntityType.COW);
		if (cow == null) {
			return true;
		}
		cow.setVelocity(location.getDirection().multiply(-2));
		new Remove(cow).runTaskLater(plugin, 20);
		return true;
	}

	private class Remove extends BukkitRunnable {
		private Entity entity;

		public Remove(Entity entity) {
			this.entity = entity;
		}

		@Override
		public void run() {
			entity.remove();
		}
	}
}
