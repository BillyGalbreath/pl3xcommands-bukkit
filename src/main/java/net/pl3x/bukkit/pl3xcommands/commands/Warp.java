package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.WarpConfig;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class Warp implements TabExecutor {
	private Main plugin;

	public Warp(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "warp");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			return getMatchingWarpNames(args[0]);
		}
		return null;
	}

	public static List<String> getMatchingWarpNames(String name) {
		List<String> list = new ArrayList<String>();
		for (String warp : WarpConfig.getConfig().getWarps()) {
			if (warp.toLowerCase().startsWith(name.toLowerCase())) {
				list.add(warp);
			}
		}
		return list;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.warp")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (!(sender instanceof Player) && args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		WarpConfig config = WarpConfig.getConfig();
		if (args.length < 1) {
			List<String> warps = config.getWarps();
			if (warps == null || warps.isEmpty()) {
				plugin.sendMessage(sender, Lang.WARP_LIST_EMPTY.get());
				return true;
			}
			plugin.sendMessage(sender, Lang.WARP_LIST.get().replace("{list}", warps.toString()));
			return true;
		}
		String warpName = args[0].toLowerCase();
		Location warp = config.getWarp(warpName);
		if (warp == null) {
			plugin.sendMessage(sender, Lang.WARP_DOES_NOT_EXIST.get());
			return true;
		}
		if (args.length == 1) {
			Player player = (Player) sender;
			if (Config.USE_WARP_PERMS.getBoolean() && !sender.hasPermission("command.warp." + warpName)) {
				plugin.sendMessage(sender, Lang.WARP_NO_PERM.get());
				return true;
			}
			plugin.sendMessage(sender, Lang.WARP_GOING_TO.get().replace("{warp}", warpName));
			player.teleport(warp);
			return true;
		}
		if (!sender.hasPermission("others.warp")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[1]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.warp")) {
			plugin.sendMessage(sender, Lang.WARP_EXEMPT.get());
			return true;
		}
		plugin.sendMessage(sender, Lang.WARP_OTHER.get().replace("{target}", target.getName()).replace("{warp}", warpName));
		plugin.sendMessage(target, Lang.WARP_OTHER_NOTICE.get().replace("{player}", sender.getName()).replace("{warp}", warpName));
		target.teleport(warp);
		return true;
	}
}
