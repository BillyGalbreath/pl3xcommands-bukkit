package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Burn implements CommandExecutor {
	private Main plugin;

	public Burn(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "burn");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.burn")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.burn")) {
			plugin.sendMessage(sender, Lang.BURN_EXEMPT.get());
			return true;
		}
		int seconds = 5;
		if (args.length > 1) {
			try {
				seconds = Integer.valueOf(args[1]);
			} catch (NumberFormatException e) {
				plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
				return true;
			}
		}
		if (seconds <= 0) {
			plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
			return true;
		}
		String time = DurationFormatUtils.formatDurationWords(seconds * 1000, true, true);
		plugin.sendMessage(sender, Lang.BURN.get().replace("{player}", target.getName()).replace("{time}", time));
		target.setFireTicks(seconds * 20);
		return true;
	}
}
