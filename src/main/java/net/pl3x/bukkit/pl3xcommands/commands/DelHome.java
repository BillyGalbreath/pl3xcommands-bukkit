package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class DelHome implements TabExecutor {
	private Main plugin;

	public DelHome(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "delhome");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return null;
		}
		if (args.length == 1) {
			return Home.getMatchingHomeNames((Player) sender, args[0]);
		}
		return null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.delhome")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (args.length < 1) {
			plugin.sendMessage(sender, Lang.HOME_DELETE_DEFAULT.get());
			return true;
		}
		PlayerConfig config = PlayerConfig.getConfig(player);
		String home = args[0];
		if (config.get("home." + home) == null) {
			plugin.sendMessage(sender, Lang.HOME_DOES_NOT_EXIST.get());
			return true;
		}
		config.set("home." + home, null);
		plugin.sendMessage(sender, Lang.HOME_DELETE.get().replace("{home}", home));
		return true;
	}
}
