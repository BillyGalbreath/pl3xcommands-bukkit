package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Speak implements CommandExecutor {
	private Main plugin;

	public Speak(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "speak");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.speak")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (args[1].startsWith("/")) {
			plugin.sendMessage(sender, Lang.SPEAK_NO_COMMANDS.get());
			return true;
		}
		if (target.hasPermission("exempt.speak")) {
			plugin.sendMessage(sender, Lang.SPEAK_EXEMPT.get());
			return true;
		}
		target.chat(Utils.join(args, " ", 1));
		return true;
	}
}
