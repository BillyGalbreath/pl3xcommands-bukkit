package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.SpawnConfig;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Compass implements CommandExecutor {
	private Main plugin;

	public Compass(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "compass");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.compass")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		String command = args[0];
		if (args.length < 2) {
			if (command.equalsIgnoreCase("here")) {
				player.setCompassTarget(player.getLocation());
				plugin.sendMessage(sender, Lang.COMPASS_CURRENT_POSITION.get());
				return true;
			}
			if (command.equalsIgnoreCase("reset")) {
				Location bedSpawn = player.getBedSpawnLocation();
				Location worldSpawn = SpawnConfig.getConfig().getSpawn(player.getWorld());
				player.setCompassTarget(bedSpawn == null ? worldSpawn : bedSpawn);
				plugin.sendMessage(sender, Lang.COMPASS_RESET.get());
				return true;
			}
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		if (command.equalsIgnoreCase("player")) {
			Player target = Bukkit.getPlayer(args[1]);
			if (target == null) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
				return true;
			}
			player.setCompassTarget(target.getLocation());
			plugin.sendMessage(sender, Lang.COMPASS_POINT_PLAYER.get().replace("{player}", target.getName()));
			return true;
		}
		if (command.equalsIgnoreCase("location")) {
			if (args.length < 4) {
				plugin.sendMessage(sender, cmd.getDescription());
				return false;
			}
			double x;
			double y;
			double z;
			try {
				x = Double.valueOf(args[1]);
				y = Double.valueOf(args[2]);
				z = Double.valueOf(args[3]);
			} catch (Exception e) {
				plugin.sendMessage(sender, Lang.COMPASS_INVALID_COORDS.get());
				return true;
			}
			player.setCompassTarget(new Location(player.getWorld(), x, y, z));
			plugin.sendMessage(sender, Lang.COMPASS_POINT_COORDS.get().replace("{x}", Double.toString(x)).replace("{y}", Double.toString(y)).replace("{z}", Double.toString(z)));
			return true;
		}
		plugin.sendMessage(sender, cmd.getDescription());
		return false;
	}
}
