package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Starve implements CommandExecutor {
	private Main plugin;

	public Starve(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "starve");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.starve")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.starve")) {
			plugin.sendMessage(sender, Lang.STARVE_EXEMPT.get());
			return true;
		}
		int amount;
		try {
			amount = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
			return true;
		}
		if (amount > 20 || amount < 1) {
			plugin.sendMessage(sender, Lang.ERROR_NUMBER_NOT_BETWEEN.get().replace("{number1}", "1").replace("{number2}", "20"));
			return true;
		}
		target.setFoodLevel(target.getFoodLevel() - amount);
		plugin.sendMessage(sender, Lang.STARVE.get().replace("{player}", target.getName()));
		plugin.sendMessage(target, Lang.STARVE_NOTICE.get().replace("{player}", sender.getName()));
		return true;
	}
}
