package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.apache.commons.lang.BooleanUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

public class Fly implements CommandExecutor {
	private Main plugin;

	public Fly(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "fly")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.fly")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return true;
			}
			Player player = (Player) sender;
			boolean fly = !player.getAllowFlight();
			player.setAllowFlight(fly);
			player.setFlying(false);
			plugin.sendMessage(sender, Lang.FLY.get().replace("{action}", BooleanUtils.toStringOnOff(fly)));
			return true;
		}
		if (!sender.hasPermission("others.fly")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.fly")) {
			plugin.sendMessage(sender, Lang.FLY_EXEMPT.get());
			return true;
		}
		boolean fly = !target.getAllowFlight();
		target.setAllowFlight(fly);
		target.setFlying(false);
		plugin.sendMessage(sender, Lang.FLY_OTHER.get().replace("{player}", target.getName()).replace("{action}", BooleanUtils.toStringOnOff(fly)));
		plugin.sendMessage(target, Lang.FLY_NOTICE.get().replace("{action}", BooleanUtils.toStringOnOff(fly)));
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
			Player player = event.getPlayer();
			player.setFlying(false);
			player.setAllowFlight(false);
		}
	}
}
