package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Ride implements CommandExecutor {
	private Main plugin;

	public Ride(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "ride");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.ride")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (args.length < 1) {
			Entity vehicle = player.getVehicle();
			Entity passenger = player.getPassenger();
			if (vehicle == null && passenger == null) {
				player.sendMessage(cmd.getDescription());
				return false;
			}
			if (vehicle != null) {
				vehicle.eject();
			}
			if (passenger != null) {
				player.eject();
			}
			plugin.sendMessage(player, Lang.RIDE_EJECT.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (player.getUniqueId().equals(target.getUniqueId())) {
			plugin.sendMessage(sender, Lang.RIDE_SELF.get());
			return true;
		}
		if (target.hasPermission("exempt.ride")) {
			plugin.sendMessage(sender, Lang.RIDE_EXEMPT.get());
			return true;
		}
		target.setPassenger(player);
		return true;
	}
}
