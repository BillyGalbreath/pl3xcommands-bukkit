package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearInventory implements CommandExecutor {
	private Main plugin;

	public ClearInventory(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "clearinventory");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.clearinventory")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return true;
			}
			((Player) sender).getInventory().clear();
			plugin.sendMessage(sender, Lang.CLEARINVENTORY.get());
			return true;
		}
		if (!sender.hasPermission("others.clearinventory")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.clearinventory")) {
			plugin.sendMessage(sender, Lang.CLEARINVENTORY_EXEMPT.get());
			return true;
		}
		plugin.sendMessage(sender, Lang.CLEARINVENTORY_OTHER.get().replace("{player}", target.getName()));
		plugin.sendMessage(target, Lang.CLEARINVENTORY_NOTICE.get());
		target.getInventory().clear();
		return true;
	}
}
