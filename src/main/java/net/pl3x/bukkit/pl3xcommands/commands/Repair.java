package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Repair implements CommandExecutor {
	private Main plugin;

	public Repair(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "repair");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.repair")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (args.length > 0 && args[0].equalsIgnoreCase("all")) {
			for (ItemStack item : player.getInventory().getContents()) {
				if (item == null || item.getType().equals(Material.AIR) || item.getDurability() == 0) {
					continue;
				}
				item.setDurability((short) 0);
			}
			plugin.sendMessage(sender, Lang.REPAIR_ALL.get());
			return true;
		}
		ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getType().equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.ERROR_NOTHING_IN_HAND.get());
			return true;
		}
		if (hand.getDurability() == 0) {
			plugin.sendMessage(sender, Lang.REPAIR_NOT_NEEDED.get());
			return true;
		}
		hand.setDurability((short) 0);
		plugin.sendMessage(sender, Lang.REPAIR.get());
		return true;
	}
}
