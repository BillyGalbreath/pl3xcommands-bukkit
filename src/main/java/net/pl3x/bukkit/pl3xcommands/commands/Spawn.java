package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xcommands.configuration.SpawnConfig;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Spawn implements CommandExecutor {
	private Main plugin;

	public Spawn(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "spawn")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.spawn")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		World world;
		if (args.length > 0) {
			if (!sender.hasPermission("command.spawn.changeworld")) {
				plugin.sendMessage(sender, Lang.SPAWN_NO_CHANGE_WORLD.get());
				return true;
			}
			world = plugin.getServer().getWorld(args[0]);
			if (world == null) {
				plugin.sendMessage(sender, Lang.ERROR_WORLD_DOES_NOT_EXIST.get());
				return true;
			}
		} else {
			world = player.getWorld();
		}
		Location spawn = SpawnConfig.getConfig().getSpawn(world);
		if (spawn == null) {
			spawn = Utils.getSafeLocation(world.getSpawnLocation());
		}
		plugin.sendMessage(sender, Lang.SPAWN.get().replace("{world}", world.getName()));
		player.teleport(spawn);
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onPlayerJoin(PlayerJoinEvent event) {
			Player player = event.getPlayer();
			PlayerConfig config = PlayerConfig.getConfig(player);
			Location lastLocation = config.getLocation("last-location");
			if (lastLocation == null) {
				config.set(player.getName(), "name");
				config.set("allow-tp", true);
				config.forceSave();
				lastLocation = SpawnConfig.getConfig().getSpawn(player.getWorld());
			}
			if (lastLocation == null) {
				lastLocation = player.getWorld().getSpawnLocation();
			}
			lastLocation = Utils.getSafeLocation(lastLocation);
			player.teleport(lastLocation);
			new Teleport(player, lastLocation).runTaskLater(plugin, 2); // this removes the "default" location from the player's /back history.
		}
	}

	public static class Teleport extends BukkitRunnable {
		private Entity entity;
		private Location location;

		public Teleport(Entity entity, Location location) {
			this.entity = entity;
			this.location = location;
		}

		@Override
		public void run() {
			entity.teleport(location);
		}
	}
}
