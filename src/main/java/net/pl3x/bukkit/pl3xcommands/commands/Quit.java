package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class Quit implements CommandExecutor {
	private Main plugin;

	public Quit(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "quit")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.quit")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		player.kickPlayer(Lang.QUIT.get() + "\00-silent");
		plugin.sendBroadcast(Lang.QUIT_BROADCAST.get().replace("{player}", player.getName()));
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void silentKicks(PlayerKickEvent event) {
			String reason = event.getReason();
			if (!reason.endsWith("\00-silent")) {
				return;
			}
			event.setLeaveMessage(null);
			event.setReason(reason.replace("\00-silent", ""));
		}

		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void savePosition(PlayerQuitEvent event) {
			Player player = event.getPlayer();
			PlayerConfig config = PlayerConfig.getConfig(player);
			config.setLocation("last-location", player.getLocation());
			config.discard();
		}

		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void savePosition(PlayerCommandPreprocessEvent event) {
			if (!event.getMessage().equalsIgnoreCase("/stop")) {
				return;
			}
			event.setCancelled(true);
			for (Player player : Bukkit.getOnlinePlayers()) {
				player.kickPlayer(Lang.QUIT_SHUTDOWN.get());
			}
			Bukkit.shutdown();
		}

		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void savePosition(ServerCommandEvent event) {
			if (!event.getCommand().equalsIgnoreCase("stop")) {
				return;
			}
			event.getSender().sendMessage(Lang.QUIT_CONSOLE_STOP.get());
			event.setCommand("list");
			for (Player player : Bukkit.getOnlinePlayers()) {
				player.kickPlayer(Lang.QUIT_SHUTDOWN.get());
			}
			Bukkit.shutdown();
		}
	}
}
