package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.Random;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class KittyCannon implements CommandExecutor {
	private Main plugin;
	private static Random random = new Random();

	public KittyCannon(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "kittycannon");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.kittycannon")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		Ocelot ocelot = (Ocelot) player.getWorld().spawnEntity(player.getLocation(), EntityType.OCELOT);
		if (ocelot == null) {
			return true;
		}
		ocelot.setCatType(Ocelot.Type.values()[random.nextInt(Ocelot.Type.values().length)]);
		if (random.nextInt(2) == 1) {
			ocelot.setBaby();
		} else {
			ocelot.setAdult();
		}
		ocelot.setTamed(true);
		ocelot.setVelocity(player.getEyeLocation().getDirection().multiply(2));
		new Explode(ocelot).runTaskLater(plugin, 20);
		return true;
	}

	private class Explode extends BukkitRunnable {
		private Entity entity;

		public Explode(Entity entity) {
			this.entity = entity;
		}

		@Override
		public void run() {
			Location location = entity.getLocation();
			entity.remove();
			location.getWorld().createExplosion(location, 0F);
		}
	}
}
