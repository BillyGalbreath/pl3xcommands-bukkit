package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpDeny implements CommandExecutor {
	private Main plugin;

	public TpDeny(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "tpdeny");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.tpdeny")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (Tpa.hasPending(player)) {
			UUID uuid = Tpa.getPending(player);
			Player target = Bukkit.getPlayer(uuid);
			Tpa.removeRequest(player);
			plugin.sendMessage(sender, Lang.TPDENY.get());
			if (target != null) {
				plugin.sendMessage(target, Lang.TPDENY_NOTICE.get());
			}
			return true;
		}
		if (TpaHere.hasPending(player)) {
			UUID uuid = TpaHere.getPending(player);
			Player target = Bukkit.getPlayer(uuid);
			TpaHere.removeRequest(player);
			plugin.sendMessage(sender, Lang.TPDENY.get());
			if (target != null) {
				plugin.sendMessage(target, Lang.TPDENY_NOTICE.get());
			}
			return true;
		}
		plugin.sendMessage(sender, Lang.TPACCEPT_NONE_PENDING.get());
		return true;
	}
}
