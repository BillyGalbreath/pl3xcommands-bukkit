package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashMap;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Reply implements CommandExecutor {
	private Main plugin;
	private static HashMap<UUID, UUID> replydb = new HashMap<UUID, UUID>();

	public Reply(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "reply");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.reply")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		if (!replydb.containsKey(player.getUniqueId())) {
			plugin.sendMessage(sender, Lang.MESSAGE_NO_REPLY.get());
			return true;
		}
		Player target = Bukkit.getPlayer(replydb.get(player.getUniqueId()));
		if (target == null || !target.isOnline()) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		String message = Utils.join(args, " ", 0);
		if (message == null || message.equals("") || ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', message)).equals("")) {
			plugin.sendMessage(sender, Lang.MESSAGE_NULL.get());
			return true;
		}
		if (Ignore.isIgnoring(target, player)) {
			plugin.sendMessage(sender, Lang.IGNORE_MESSAGE.get());
			return true;
		}
		Reply.addToReplyDB(player, target);
		plugin.sendMessage(sender, Lang.MESSAGE_SENDER.get().replace("{sender}", sender.getName()).replace("{target}", target.getName()).replace("{message}", message));
		plugin.sendMessage(target, Lang.MESSAGE_TARGET.get().replace("{sender}", sender.getName()).replace("{target}", target.getName()).replace("{message}", message));
		for (Player spy : Bukkit.getOnlinePlayers()) {
			if (!Spy.isSpy(spy)) {
				continue;
			}
			if (target.getName().equals(spy.getName())) {
				continue;
			}
			if (sender.getName().equals(spy.getName())) {
				continue;
			}
			plugin.sendMessage(spy, Lang.MESSAGE_SPY.get().replace("{sender}", sender.getName()).replace("{target}", target.getName()).replace("{message}", message));
		}
		return true;
	}

	public static void addToReplyDB(Player sender, Player target) {
		replydb.put(target.getUniqueId(), sender.getUniqueId());
		replydb.put(sender.getUniqueId(), target.getUniqueId());
	}
}
