package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Rocket implements CommandExecutor {
	private Main plugin;

	public Rocket(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "rocket");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.rocket")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.rocket")) {
			plugin.sendMessage(sender, Lang.ROCKET_EXEMPT.get());
			return true;
		}
		double x = target.getVelocity().getX();
		double y = 4;
		double z = target.getVelocity().getZ();
		target.setVelocity(new Vector(x, y, z));
		plugin.sendMessage(sender, Lang.ROCKET.get().replace("{player}", target.getName()));
		return true;
	}
}
