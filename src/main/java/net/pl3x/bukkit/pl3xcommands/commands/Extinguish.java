package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Extinguish implements CommandExecutor {
	private Main plugin;

	public Extinguish(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "extinguish");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.extinguish")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return true;
			}
			plugin.sendMessage(sender, Lang.EXTINGUISH.get());
			((Player) sender).setFireTicks(0);
			return true;
		}
		if (!sender.hasPermission("others.extinguish")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		plugin.sendMessage(sender, Lang.EXTINGUISH_FOR.get().replace("{player}", target.getName()));
		plugin.sendMessage(target, Lang.EXTINGUISH_BY.get().replace("{player}", sender.getName()));
		target.setFireTicks(0);
		return true;
	}
}
