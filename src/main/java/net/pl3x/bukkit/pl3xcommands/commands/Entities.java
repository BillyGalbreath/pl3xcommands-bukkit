package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Entities implements CommandExecutor {
	private Main plugin;

	public Entities(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "entities");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.fixchunk")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		double radius = Config.DEFAULT_NEAR_RADIUS.getDouble();
		if (args.length > 0) {
			try {
				radius = Double.parseDouble(args[0]);
			} catch (Exception e) {
				plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
				return true;
			}
			if (radius < 1) {
				plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
				return true;
			}
			if (radius > Config.MAX_NEAR_RADIUS.getDouble()) {
				plugin.sendMessage(sender, Lang.ERROR_NUNBER_TOO_BIG.get());
				return true;
			}
		}
		int amount = 0;
		for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
			if (entity instanceof Player) {
				continue;
			}
			double distance = player.getLocation().distance(entity.getLocation());
			plugin.sendMessage(sender, Lang.ENTITIES_DISTANCE.get().replace("{entity}", entity.getType().getName()).replace("{distance}", Double.toString(distance)));
			amount++;
		}
		if (amount == 0) {
			plugin.sendMessage(sender, Lang.ENTITIES_NONE.get());
		} else {
			plugin.sendMessage(sender, Lang.ENTITIES_TOTAL.get().replace("{total}", Integer.toString(amount)));
		}
		return true;
	}
}
