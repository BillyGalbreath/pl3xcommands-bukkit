package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Heal implements CommandExecutor {
	private Main plugin;

	public Heal(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "heal");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.heal")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return true;
			}
			Player player = (Player) sender;
			player.setHealth(player.getMaxHealth());
			plugin.sendMessage(sender, Lang.HEAL.get());
			return true;
		}
		if (!sender.hasPermission("others.heal")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		target.setHealth(target.getMaxHealth());
		plugin.sendMessage(sender, Lang.HEAL_OTHER.get().replace("{player}", target.getName()));
		plugin.sendMessage(target, Lang.HEAL_NOTICE.get().replace("{player}", sender.getName()));
		return true;
	}
}
