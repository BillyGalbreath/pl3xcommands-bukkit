package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Spy implements CommandExecutor {
	private Main plugin;
	private static Set<UUID> spies = new HashSet<UUID>();

	public Spy(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "spy")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.spy")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		PlayerConfig config = PlayerConfig.getConfig(player);
		boolean spy = !config.getBoolean("spy", false);
		if (spy) {
			plugin.sendMessage(sender, Lang.SPY_ENABLED.get());
			spies.add(player.getUniqueId());
		} else {
			plugin.sendMessage(sender, Lang.SPY_DISABLED.get());
			spies.remove(player.getUniqueId());
		}
		config.set("spy", spy);
		config.forceSave();
		return true;
	}

	public static boolean isSpy(Player player) {
		return spies.contains(player.getUniqueId());
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void onPlayerJoin(PlayerJoinEvent event) {
			UUID uuid = event.getPlayer().getUniqueId();
			if (PlayerConfig.getConfig(uuid).getBoolean("spy", false)) {
				spies.add(uuid);
			} else {
				spies.remove(uuid);
			}
		}
	}
}
