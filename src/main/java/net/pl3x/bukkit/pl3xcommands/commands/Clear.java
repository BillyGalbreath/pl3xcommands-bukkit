package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class Clear implements CommandExecutor {
	private Main plugin;

	public Clear(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "clear");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.clear")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, Lang.CLEAR_LIST.get().replace("{list}", getEntityList()));
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		int radius = -1;
		try {
			radius = Integer.valueOf(args[1]);
		} catch (Exception e) {
			plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
			return true;
		}
		if (radius < 0) {
			plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
			return true;
		}
		String typeStr = args[0].toUpperCase().trim();
		EntityType entityType = null;
		try {
			entityType = EntityType.valueOf(typeStr);
		} catch (Exception ignore) {
		}
		if (entityType == null && !typeStr.equalsIgnoreCase("all")) {
			plugin.sendMessage(sender, Lang.CLEAR_UNKNOWN_TYPE.get());
			return true;
		}
		Player player = (Player) sender;
		List<Entity> entityList = player.getNearbyEntities(radius, radius, radius);
		if (entityList.isEmpty()) {
			plugin.sendMessage(sender, Lang.CLEAR_NONE_NEAR.get());
			return true;
		}
		int count = 0;
		if (entityType == null) {
			for (EntityType type : EntityType.values()) {
				count += clear(entityList, type);
			}
		} else {
			count = clear(entityList, entityType);
		}
		plugin.sendMessage(sender, Lang.CLEAR.get().replace("{type}", entityType == null ? "all" : entityType.name().toLowerCase()).replace("{count}", Integer.toString(count)));
		return true;
	}

	private int clear(List<Entity> list, EntityType type) {
		int count = 0;
		for (Entity entity : list) {
			if (entity.getType().equals(type)) {
				entity.remove();
				count++;
			}
		}
		return count;
	}

	private String getEntityList() {
		String value = "";
		for (EntityType type : EntityType.values()) {
			if (value != null) {
				value += ", ";
			}
			value += type.name();
		}
		return value.toLowerCase();
	}
}
