package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpToggle implements CommandExecutor {
	private Main plugin;

	public TpToggle(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "tptoggle");
	}

	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.tptoggle")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		PlayerConfig config = PlayerConfig.getConfig(player);
		if (config.getBoolean("allow-tp")) {
			config.set("allow-tp", false);
			config.forceSave();
			plugin.sendMessage(sender, Lang.TPTOGGLE_DISABLED.get());
			return true;
		}
		config.set("allow-tp", true);
		config.forceSave();
		plugin.sendMessage(sender, Lang.TPTOGGLE_ENABLED.get());
		return true;
	}

	public static boolean isTpAllowed(Player player) {
		return PlayerConfig.getConfig(player).getBoolean("allow-tp", true);
	}
}
