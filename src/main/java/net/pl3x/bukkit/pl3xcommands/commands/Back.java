package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashMap;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class Back implements CommandExecutor {
	private Main plugin;
	private HashMap<UUID, Location> backdb = new HashMap<UUID, Location>();

	public Back(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "back")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public Location getPreviousLocation(Player player) {
		return backdb.get(player.getUniqueId());
	}

	public void setPreviousLocation(Player player, Location location) {
		backdb.put(player.getUniqueId(), location);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.back")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		Location back = getPreviousLocation(player);
		if (back == null) {
			plugin.sendMessage(sender, Lang.BACK_NOT_FOUND.get());
			return true;
		}
		player.teleport(back);
		plugin.sendMessage(sender, Lang.BACK_TELEPORTING.get());
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void backOnDeath(PlayerDeathEvent event) {
			if (!Config.BACK_ON_DEATH.getBoolean()) {
				return;
			}
			Player player = event.getEntity();
			if (!player.hasPermission("command.back")) {
				return;
			}
			setPreviousLocation(player, player.getLocation());
			plugin.sendMessage(player, Lang.BACK_DEATH_HINT.get());
		}

		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void backOnTeleport(PlayerTeleportEvent event) {
			Player player = event.getPlayer();
			if (!player.hasPermission("command.back")) {
				return;
			}
			setPreviousLocation(player, player.getLocation());
		}
	}
}
