package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FakeOp implements CommandExecutor {
	private Main plugin;

	public FakeOp(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "fakeop");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.fakeop")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player victim = Bukkit.getPlayer(args[0]);
		if (victim == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		plugin.sendMessage(victim, Lang.FAKEOP_VICTIM.get());
		plugin.sendMessage(sender, Lang.FAKEOP.get().replace("{victim}", victim.getName()));
		return true;
	}
}
