package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Feed implements CommandExecutor {
	private Main plugin;

	public Feed(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "feed");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.feed")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
				return false;
			}
			Player player = (Player) sender;
			player.setFoodLevel(20);
			player.setSaturation(20);
			plugin.sendMessage(player, Lang.FEED.get());
			return true;
		}
		if (!sender.hasPermission("others.feed")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.feed")) {
			plugin.sendMessage(sender, Lang.GAMEMODE_EXEMPT.get());
			return true;
		}
		target.setFoodLevel(20);
		target.setSaturation(20F);
		plugin.sendMessage(sender, Lang.FEED_OTHER.get().replace("{player}", target.getName()));
		plugin.sendMessage(target, Lang.FEED_NOTICE.get().replace("{player}", sender.getName()));
		return true;
	}
}
