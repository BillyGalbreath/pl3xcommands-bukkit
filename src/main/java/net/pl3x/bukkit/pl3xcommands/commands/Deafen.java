package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Deafen implements CommandExecutor {
	private Main plugin;

	public Deafen(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "deafen")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.deafen")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.deafen")) {
			plugin.sendMessage(sender, Lang.DEAFEN_EXEMPT.get());
			return true;
		}
		PlayerConfig config = PlayerConfig.getConfig(target);
		Boolean deafen = !config.getBoolean("deaf", false);
		config.set("deaf", deafen);
		config.forceSave();
		if (deafen) {
			plugin.sendMessage(sender, Lang.DEAFEN_ON.get().replace("{player}", target.getName()));
		} else {
			plugin.sendMessage(sender, Lang.DEAFEN_OFF.get().replace("{player}", target.getName()));
		}
		return true;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void deafen(AsyncPlayerChatEvent event) {
			List<Player> toRemove = new ArrayList<Player>();
			for (Player recipient : event.getRecipients()) {
				if (event.getPlayer().getUniqueId().equals(recipient.getUniqueId())) {
					continue;
				}
				Boolean isDeaf = PlayerConfig.getConfig(recipient).getBoolean("deaf");
				if (isDeaf == null || !isDeaf) {
					continue;
				}
				toRemove.add(recipient);
			}
			event.getRecipients().removeAll(toRemove); // may or may not work (according to API docs)
			// do not cancel this event!
		}
	}
}
