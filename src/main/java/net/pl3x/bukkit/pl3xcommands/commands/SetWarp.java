package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.WarpConfig;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class SetWarp implements TabExecutor {
	private Main plugin;

	public SetWarp(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "setwarp");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 1) {
			return Warp.getMatchingWarpNames(args[0]);
		}
		return null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.setwarp")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		String warp = args[0].toLowerCase();
		WarpConfig.getConfig().setWarp(warp, player.getLocation());
		plugin.sendMessage(sender, Lang.WARP_SET.get().replace("{warp}", warp));
		return true;
	}
}
