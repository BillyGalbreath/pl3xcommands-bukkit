package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.ChatManager;
import net.pl3x.bukkit.pl3xcommands.ChatManager.Channel;
import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Global implements CommandExecutor {
	private Main plugin;

	public Global(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "global");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.global")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		ChatManager.getManager().setChannel(player, Channel.GLOBAL);
		player.chat(Utils.join(args, " ", 0));
		return true;
	}
}
