package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Message implements CommandExecutor {
	private Main plugin;

	public Message(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "message");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.message")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		String message = Utils.join(args, " ", 1);
		if (message == null || message.equals("") || ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', message)).equals("")) {
			plugin.sendMessage(sender, Lang.MESSAGE_NULL.get());
			return true;
		}
		if (sender instanceof Player) {
			if (Ignore.isIgnoring(target, (Player) sender)) {
				plugin.sendMessage(sender, Lang.IGNORE_MESSAGE.get());
				return true;
			}
			Reply.addToReplyDB((Player) sender, target);
		}
		plugin.sendMessage(sender, Lang.MESSAGE_SENDER.get().replace("{sender}", sender.getName()).replace("{target}", target.getName()).replace("{message}", message));
		plugin.sendMessage(target, Lang.MESSAGE_TARGET.get().replace("{sender}", sender.getName()).replace("{target}", target.getName()).replace("{message}", message));
		for (Player spy : Bukkit.getOnlinePlayers()) {
			if (!Spy.isSpy(spy)) {
				continue;
			}
			if (target.getName().equals(spy.getName())) {
				continue;
			}
			if (sender.getName().equals(spy.getName())) {
				continue;
			}
			plugin.sendMessage(spy, Lang.MESSAGE_SPY.get().replace("{sender}", sender.getName()).replace("{target}", target.getName()).replace("{message}", message));
		}
		return true;
	}
}
