package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FixChunk implements CommandExecutor {
	private Main plugin;

	public FixChunk(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "fixchunk");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.fixchunk")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		Chunk chunk = player.getLocation().getChunk();
		if (!chunk.isLoaded()) {
			chunk.load(true);
		}
		boolean worked = player.getWorld().refreshChunk(chunk.getX(), chunk.getZ());
		chunk.unload();
		chunk.load();
		if (worked) {
			plugin.sendMessage(sender, Lang.FIXCHUNK.get());
		} else {
			plugin.sendMessage(sender, Lang.FIXCHUNK_ERROR.get());
		}
		return true;
	}
}
