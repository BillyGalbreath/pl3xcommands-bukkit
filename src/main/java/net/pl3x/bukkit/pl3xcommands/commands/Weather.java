package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Weather implements CommandExecutor {
	private Main plugin;

	public Weather(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "weather");
	}

	public enum Condition {
		SUN(false, false),
		RAIN(true, false),
		STORM(true, true);

		private boolean storm = false;
		private boolean thunder = false;

		Condition(boolean storm, boolean thunder) {
			this.storm = storm;
			this.thunder = thunder;
		}

		public void set(World world) {
			set(world, null);
		}

		public void set(World world, Integer duration) {
			world.setStorm(storm);
			world.setThundering(thunder);
			if (duration == null) {
				return;
			}
			world.setThunderDuration(duration);
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.weather")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		Condition condition = null;
		try {
			condition = Condition.valueOf(args[0].toUpperCase());
		} catch (Exception ignore) {
		}
		if (condition == null) {
			plugin.sendMessage(sender, Lang.WEATHER_INVALID.get());
			return true;
		}
		if (args.length == 1) {
			condition.set(player.getWorld());
			return true;
		}
		int duration;
		try {
			duration = Integer.valueOf(args[1]);
		} catch (NumberFormatException e) {
			plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
			return true;
		}
		if (duration < 1) {
			plugin.sendMessage(sender, Lang.ERROR_NOMBER_NOT_POSITIVE.get());
			return true;
		}
		condition.set(player.getWorld(), duration);
		return true;
	}
}
