package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteAll implements CommandExecutor {
	private Main plugin;

	public MuteAll(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "muteall");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.muteall")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		boolean mute = label.startsWith("un") || label.startsWith("UN");
		for (Player target : Bukkit.getOnlinePlayers()) {
			if (target.hasPermission("exempt.mute")) {
				continue;
			}
			if (sender.getName().equals(target.getName())) {
				continue;
			}
			PlayerConfig config = PlayerConfig.getConfig(target);
			if (mute) {
				config.set("muted", true);
				config.forceSave();
				plugin.sendMessage(target, Lang.MUTEALL_NOTICE.get());
			} else {
				config.set("muted", false);
				config.forceSave();
				plugin.sendMessage(target, Lang.MUTEALL_NOTICE_UNMUTE.get());
			}
		}
		if (mute) {
			plugin.sendMessage(sender, Lang.MUTEALL.get());
		} else {
			plugin.sendMessage(sender, Lang.MUTEALL_UNMUTE.get());
		}
		return true;
	}
}
