package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashMap;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Tpa implements CommandExecutor {
	private Main plugin;
	private static HashMap<UUID, UUID> tpadb = new HashMap<UUID, UUID>();

	public Tpa(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "tpa");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.tpa")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (!TpToggle.isTpAllowed(target) && !player.hasPermission("command.tp.override")) {
			plugin.sendMessage(sender, Lang.TPTOGGLE_OFF.get());
			return true;
		}
		String senderUUID = ((Player) sender).getUniqueId().toString();
		for (String ignored : PlayerConfig.getConfig(target).getStringList("ignored")) {
			if (ignored.equals(senderUUID)) {
				plugin.sendMessage(sender, Lang.IGNORE_MESSAGE.get());
				return true;
			}
		}
		sendRequest(target, player);
		plugin.sendMessage(sender, Lang.TPA_SENT.get().replace("{player}", target.getName()));
		return true;
	}

	public static void sendRequest(Player target, Player sender) {
		tpadb.put(target.getUniqueId(), sender.getUniqueId());
		Main.getInstance().sendMessage(target, Lang.TPA_NOTICE1.get().replace("{player}", sender.getName()));
		Main.getInstance().sendMessage(target, Lang.TPA_NOTICE2.get());
	}

	public static void removeRequest(Player target) {
		tpadb.remove(target.getUniqueId());
	}

	public static boolean hasPending(Player target) {
		return tpadb.containsKey(target.getUniqueId());
	}

	public static UUID getPending(Player target) {
		return tpadb.get(target.getUniqueId());
	}
}
