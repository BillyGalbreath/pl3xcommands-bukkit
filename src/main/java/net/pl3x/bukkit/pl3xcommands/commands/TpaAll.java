package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpaAll implements CommandExecutor {
	private Main plugin;

	public TpaAll(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "tpaall");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.tpaall")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		for (Player target : Bukkit.getOnlinePlayers()) {
			if (target.getUniqueId().equals(player.getUniqueId())) {
				continue;
			}
			if (!TpToggle.isTpAllowed(target) && !player.hasPermission("command.tp.override")) {
				continue;
			}
			if (Ignore.isIgnoring(target, player)) {
				continue;
			}
			TpaHere.sendRequest(target, player);
		}
		plugin.sendMessage(sender, Lang.TPAALL_SENT.get());
		return true;
	}
}
