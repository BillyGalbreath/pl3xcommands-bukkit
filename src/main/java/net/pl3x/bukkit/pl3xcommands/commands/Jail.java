package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.JailConfig;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xcommands.configuration.SpawnConfig;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.PluginManager;

public class Jail implements CommandExecutor {
	private Main plugin;

	public Jail(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "jail")) {
			return;
		}
		PluginManager pluginManager = plugin.getServer().getPluginManager();
		Listener listener = new Listener() {
		};
		EventExecutor executor = new EventExecutor() {
			public void execute(Listener listener, Event event) {
				if (event instanceof PlayerEvent) {
					jailed(event, ((PlayerEvent) event).getPlayer());
				} else if (event instanceof BlockBreakEvent) {
					jailed(event, ((BlockBreakEvent) event).getPlayer());
				} else if (event instanceof BlockPlaceEvent) {
					jailed(event, ((BlockPlaceEvent) event).getPlayer());
				}
			}
		};

		List<Class<? extends Event>> eventsToListenFor = new ArrayList<Class<? extends Event>>();
		eventsToListenFor.add(BlockPlaceEvent.class);
		eventsToListenFor.add(BlockBreakEvent.class);
		eventsToListenFor.add(PlayerTeleportEvent.class);
		eventsToListenFor.add(PlayerCommandPreprocessEvent.class);

		List<EventPriority> eventPrioritiesToListenOn = new ArrayList<EventPriority>();
		eventPrioritiesToListenOn.add(EventPriority.HIGHEST);
		eventPrioritiesToListenOn.add(EventPriority.LOWEST);

		for (Class<? extends Event> iClass : eventsToListenFor) {
			for (EventPriority iPriority : eventPrioritiesToListenOn) {
				pluginManager.registerEvent(iClass, listener, iPriority, executor, plugin, true);
			}
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.jail")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		JailConfig jailConfig = JailConfig.getConfig();
		if (args.length < 1) {
			List<String> jails = jailConfig.getJails();
			if (jails == null || jails.isEmpty()) {
				plugin.sendMessage(sender, Lang.JAIL_NONE_FOUND.get());
				return true;
			}
			plugin.sendMessage(sender, Lang.JAIL_LIST.get().replace("{list}", jails.toString()));
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		PlayerConfig playerConfig = PlayerConfig.getConfig(target);
		if (playerConfig.getBoolean("jailed")) {
			playerConfig.set("jailed", false);
			playerConfig.forceSave();
			Location spawn = SpawnConfig.getConfig().getSpawn(target.getWorld());
			target.teleport(spawn);
			new Spawn.Teleport(target, spawn).runTaskLater(plugin, 2); // this removes the jail location from the player's /back history.
			plugin.sendMessage(sender, Lang.JAILED_RELEASED.get().replace("{player}", target.getDisplayName()));
			plugin.sendMessage(target, Lang.JAILED_RELEASED_NOTICE.get());
			return true;
		}
		if (args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		if (target.hasPermission("exempt.jail")) {
			plugin.sendMessage(sender, Lang.JAIL_EXEMPT.get());
			return true;
		}
		String jail = args[1].toLowerCase();
		Location loc = jailConfig.getJail(jail);
		if (loc == null) {
			plugin.sendMessage(sender, Lang.JAIL_NOT_FOUND.get());
			return true;
		}
		playerConfig.set("jailed", true);
		playerConfig.forceSave();
		target.teleport(loc);
		plugin.sendMessage(sender, Lang.JAILED.get().replace("{player}", target.getDisplayName()));
		plugin.sendMessage(target, Lang.JAILED_NOTICE.get());
		return true;
	}

	private void jailed(Event event, Player player) {
		if (!PlayerConfig.getConfig(player).getBoolean("jailed")) {
			return;
		}
		plugin.sendMessage(player, Lang.JAILED_MSG.get());
		if (event instanceof Cancellable) {
			((Cancellable) event).setCancelled(true);
		}
	}
}
