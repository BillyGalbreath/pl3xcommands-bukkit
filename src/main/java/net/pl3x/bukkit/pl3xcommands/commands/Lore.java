package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Lore implements CommandExecutor {
	private Main plugin;

	public Lore(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "lore");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.lore")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		String loreText = Utils.join(args, " ", 0);
		ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getType().equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.ERROR_NOTHING_IN_HAND.get());
			return true;
		}
		if (loreText.equalsIgnoreCase("clear")) {
			ItemMeta meta = hand.getItemMeta();
			meta.setLore(null);
			hand.setItemMeta(meta);
			player.setItemInHand(hand);
			plugin.sendMessage(sender, Lang.LORE_RESET.get());
			return true;
		}
		ItemMeta meta = hand.getItemMeta();
		List<String> lores = meta.getLore();
		if (lores == null) {
			lores = new ArrayList<String>();
		}
		lores.add(ChatColor.translateAlternateColorCodes('&', loreText));
		meta.setLore(lores);
		hand.setItemMeta(meta);
		player.setItemInHand(hand);
		plugin.sendMessage(sender, Lang.LORE.get());
		return true;
	}
}
