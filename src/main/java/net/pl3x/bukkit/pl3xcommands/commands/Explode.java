package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.HashSet;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Explode implements CommandExecutor {
	private Main plugin;

	public Explode(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "explode");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.explode")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length == 0) {
			// No args sent
			if (!(sender instanceof Player)) {
				plugin.sendMessage(sender, cmd.getDescription());
				return true;
			}
			explode(getLocation(sender), Config.DEFAULT_EXPLODE_POWER.getFloat());
			return true;
		}
		float power;
		if (args.length == 1) {
			try {
				// Arg sent is power
				power = Float.valueOf(args[0]);
				if (!(sender instanceof Player)) {
					plugin.sendMessage(sender, cmd.getDescription());
					return true;
				}
				explode(getLocation(sender), power);
				return true;
			} catch (NumberFormatException ignore) {
				// Arg sent is player
				power = Config.DEFAULT_EXPLODE_POWER.getFloat();
			}
		} else {
			try {
				// 2nd arg sent must be power
				power = Float.valueOf(args[1]);
			} catch (NumberFormatException e) {
				plugin.sendMessage(sender, Lang.ERROR_NOT_VALID_NUMBER.get());
				return true;
			}
		}
		if (!sender.hasPermission("others.explode")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		// 1st arg sent must be player
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.explode")) {
			plugin.sendMessage(sender, Lang.EXPLODE_EXEMPT.get());
			return true;
		}
		explode(target.getLocation(), power);
		plugin.sendMessage(sender, Lang.EXPLODE_PLAYER.get().replace("{player}", target.getName()));
		return true;
	}

	private Location getLocation(CommandSender sender) {
		return ((Player) sender).getTargetBlock(new HashSet<Byte>(), 120).getLocation();
	}

	private void explode(Location location, float power) {
		location.getWorld().createExplosion(location, power, Config.EXPLODE_MAKES_FIRE.getBoolean());
	}
}
