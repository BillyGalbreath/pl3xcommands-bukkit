package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Ignore implements CommandExecutor {
	private Main plugin;

	public Ignore(Main plugin) {
		this.plugin = plugin;
		if (!plugin.registerCommand(this, "ignore")) {
			return;
		}
		Bukkit.getPluginManager().registerEvents(new EventListener(), plugin);
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.ignore")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.ignore")) {
			plugin.sendMessage(sender, Lang.IGNORE_EXEMPT.get());
			return true;
		}
		UUID targetUUID = target.getUniqueId();
		PlayerConfig config = PlayerConfig.getConfig(target);
		List<String> players = config.getStringList("ignored");
		if (players == null) {
			players = new ArrayList<String>();
		}
		for (String ignored : players) {
			if (ignored.equals(targetUUID.toString())) {
				players.remove(ignored);
				config.set("ignored", players);
				config.forceSave();
				plugin.sendMessage(sender, Lang.IGNORE_REMOVE.get().replace("{player}", target.getName()));
				return true;
			}
		}
		players.add(target.getUniqueId().toString());
		config.set("ignoredby", players);
		config.forceSave();
		plugin.sendMessage(sender, Lang.IGNORE_ADD.get().replace("{player}", target.getName()));
		return true;
	}

	public static boolean isIgnoring(Player target, Player sender) {
		String senderUUID = sender.getUniqueId().toString();
		for (String ignored : PlayerConfig.getConfig(target).getStringList("ignored")) {
			if (ignored.equals(senderUUID)) {
				return true;
			}
		}
		return false;
	}

	public class EventListener implements Listener {
		@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
		public void ignored(AsyncPlayerChatEvent event) {
			PlayerConfig config = PlayerConfig.getConfig(event.getPlayer());
			if (config.get("ignoredby") == null) {
				return;
			}
			List<String> ignoredBy = config.getStringList("ignoredby");
			if (ignoredBy == null || ignoredBy.isEmpty()) {
				return;
			}
			Set<Player> ignore = new HashSet<Player>();
			for (Player recipient : event.getRecipients()) {
				for (String ignorer : ignoredBy) {
					if (recipient.getUniqueId().equals(ignorer)) {
						ignore.add(recipient);
					}
				}
			}
			event.getRecipients().removeAll(ignore); // may or may not work (according to API docs)
			// do not cancel this event!
		}
	}
}
