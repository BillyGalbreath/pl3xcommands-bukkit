package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.UUID;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpAccept implements CommandExecutor {
	private Main plugin;

	public TpAccept(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "tpaccept");
	}

	public boolean onCommand(CommandSender sender, final Command cmd, final String label, final String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.tpaccept")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		Player player = (Player) sender;
		if (Tpa.hasPending(player)) {
			UUID uuid = Tpa.getPending(player);
			Player target = Bukkit.getPlayer(uuid);
			Tpa.removeRequest(player);
			if (target != null) {
				plugin.sendMessage(sender, Lang.TPACCEPT.get());
				plugin.sendMessage(target, Lang.TPACCEPT_NOTICE.get());
				target.teleport(player);
				return true;
			}
		}
		if (TpaHere.hasPending(player)) {
			UUID uuid = TpaHere.getPending(player);
			Player target = Bukkit.getPlayer(uuid);
			TpaHere.removeRequest(player);
			if (target != null) {
				plugin.sendMessage(sender, Lang.TPACCEPT.get());
				plugin.sendMessage(target, Lang.TPACCEPT_NOTICE.get());
				player.teleport(target);
				return true;
			}
		}
		plugin.sendMessage(sender, Lang.TPACCEPT_NONE_PENDING.get());
		return true;
	}
}
