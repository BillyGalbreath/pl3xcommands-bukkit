package net.pl3x.bukkit.pl3xcommands.commands;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.Config;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Time implements CommandExecutor {
	private Main plugin;

	public Time(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "time");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.time")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}

		// Get current time
		if (args.length < 1) {
			if (!(sender instanceof Player)) {
				// Show times of all worlds to console
				for (World world : Bukkit.getWorlds()) {
					long ticks = world.getTime();
					Map<String, String> time = getTime(ticks);
					plugin.sendMessage(sender, Lang.TIME_CURRENT.get().replace("{world}", world.getName()).replace("{ticks}", time.get("ticks")).replace("{24h}", time.get("24h")).replace("{12h}", time.get("12h")));
				}
				return true;
			}
			// Show time of current world to player
			World world = ((Player) sender).getWorld();
			long ticks = world.getTime();
			Map<String, String> time = getTime(ticks);
			plugin.sendMessage(sender, Lang.TIME_CURRENT.get().replace("{world}", world.getName()).replace("{ticks}", time.get("ticks")).replace("{24h}", time.get("24h")).replace("{12h}", time.get("12h")));
			return true;
		}

		if (!sender.hasPermission("command.time.set")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}

		// remove "set" argument from command since it is not needed
		if (args[0].equalsIgnoreCase("set")) {
			args = (String[]) ArrayUtils.remove(args, 0);
		}

		// Get the effected world name(s)
		Set<World> worlds = new HashSet<World>();
		if (args.length == 2) {
			if (args[1].equalsIgnoreCase("all")) {
				worlds = new HashSet<World>(Bukkit.getWorlds());
			} else {
				World world = Bukkit.getWorld(args[1]);
				if (world == null) {
					plugin.sendMessage(sender, Lang.ERROR_WORLD_DOES_NOT_EXIST.get());
					return true;
				}
				worlds.add(world);
			}
		} else {
			if (!(sender instanceof Player)) {
				worlds = new HashSet<World>(Bukkit.getWorlds());
			} else {
				worlds.add(((Player) sender).getWorld());
			}
		}

		// Get the time to set
		Long ticks = getValidTime(args[0]);
		if (ticks == null) {
			plugin.sendMessage(sender, Lang.TIME_INVALID.get());
			return true;
		}
		Map<String, String> time = getTime(ticks);

		// Set the time
		for (World world : worlds) {
			if (Config.USE_SMOOTH_TIME_CHANGE.getBoolean()) {
				new SmoothTime(ticks, world).runTask(plugin);
			} else {
				world.setTime(ticks);
			}
			if (Config.BROADCAST_TIME_CHANGE.getBoolean()) {
				for (Player target : world.getPlayers()) {
					plugin.sendMessage(target, Lang.TIME_CHANGED.get().replace("{player}", sender.getName()).replace("{ticks}", time.get("ticks")).replace("{24h}", time.get("24h")).replace("{12h}", time.get("12h")));
				}
			}
		}

		// Acknowledge
		plugin.sendMessage(sender, Lang.TIME_SET.get().replace("{world}", worlds.size() > 1 ? "all worlds" : worlds.iterator().next().getName()).replace("{ticks}", time.get("ticks")).replace("{24h}", time.get("24h")).replace("{12h}", time.get("12h")));
		return true;
	}

	private Long getValidTime(String time) {
		try {
			long ticks = Long.valueOf(time);
			if (ticks > 24000) {
				ticks = ticks % 24000;
			}
			return ticks;
		} catch (NumberFormatException e1) {
			TimeOfDay day = null;
			try {
				day = TimeOfDay.valueOf(time.toUpperCase());
			} catch (Exception e2) {
				return null;
			}
			if (day == null) {
				return null;
			}
			return day.getTicks();
		}
	}

	private Map<String, String> getTime(long ticks) {
		HashMap<String, String> times = new HashMap<String, String>();
		if (ticks > 24000) {
			ticks = ticks % 24000;
		}
		if (ticks < 0) {
			ticks = 0;
		}
		times.put("ticks", Long.toString(ticks));
		DecimalFormat df = new DecimalFormat("00");
		df.setRoundingMode(RoundingMode.DOWN);
		float hours = ticks / 1000 + 6;
		float minutes = (ticks % 1000) * 60 / 1000;
		while (hours >= 24) {
			hours -= 24;
		}
		times.put("24h", df.format(hours) + ":" + df.format(minutes));
		String meridian = hours >= 12 ? "PM" : "AM";
		if (hours > 12) {
			hours -= 12;
		}
		if (df.format(hours).equals("00")) {
			hours = 12;
		}
		times.put("12h", df.format(hours) + ":" + df.format(minutes) + " " + meridian);
		return times;
	}

	private class SmoothTime extends BukkitRunnable {
		private long ticks;
		private World world;

		public SmoothTime(long ticks, World world) {
			if (ticks > 24000) {
				ticks = ticks % 24000;
			}
			if (ticks < 0) {
				ticks = 0;
			}
			this.ticks = ticks;
			this.world = world;
		}

		@Override
		public void run() {
			for (long i = world.getTime() + 1; i != ticks; i++) {
				if (i == 24001) {
					i = 0;
					if (ticks == 0) {
						break;
					}
				}
				world.setTime(i);
			}
			world.setTime(ticks);
		}
	}

	private enum TimeOfDay {
		DAY(0),
		MIDDAY(6000),
		NOON(6000),
		AFTERNOON(9000),
		SUNSET(12000),
		SUNDOWN(12000),
		DUSK(12000),
		NIGHT(14000),
		DARK(14000),
		MIDNIGHT(18000),
		SUNRISE(23000),
		SUNUP(23000),
		DAWN(23000),
		MORNING(23000);

		private long ticks;

		TimeOfDay(long ticks) {
			this.ticks = ticks;
		}

		public long getTicks() {
			return ticks;
		}
	}
}
