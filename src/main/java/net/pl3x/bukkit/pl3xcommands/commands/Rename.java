package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Rename implements CommandExecutor {
	private Main plugin;

	public Rename(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "rename");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return false;
		}
		if (!sender.hasPermission("command.rename")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		String newName = ChatColor.translateAlternateColorCodes('&', Utils.join(args, " ", 0));
		ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getType().equals(Material.AIR)) {
			plugin.sendMessage(sender, Lang.ERROR_NOTHING_IN_HAND.get());
			return true;
		}
		switch (hand.getType()) {
			case BREWING_STAND_ITEM:
			case BREWING_STAND:
			case DISPENSER:
			case DROPPER:
			case FURNACE:
			case BURNING_FURNACE:
			case HOPPER:
			case HOPPER_MINECART:
			case STORAGE_MINECART:
			case MONSTER_EGG:
			case CHEST:
				if (newName.length() > 32) {
					newName = newName.substring(0, 32);
				}
				plugin.sendMessage(sender, Lang.RENAME_SHORTENED_NAME.get().replace("{name}", newName));
			default:
		}
		ItemMeta meta = hand.getItemMeta();
		meta.setDisplayName(newName);
		hand.setItemMeta(meta);
		player.setItemInHand(hand);
		plugin.sendMessage(sender, Lang.RENAME.get().replace("{name}", newName));
		return true;
	}
}
