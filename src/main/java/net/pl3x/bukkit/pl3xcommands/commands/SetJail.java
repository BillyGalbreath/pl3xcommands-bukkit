package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.configuration.JailConfig;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetJail implements CommandExecutor {
	private Main plugin;

	public SetJail(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "setjail");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.setjail")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (args.length < 1) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player player = (Player) sender;
		String jail = args[0].toLowerCase();
		JailConfig config = JailConfig.getConfig();
		config.setJail(jail, player.getLocation());
		plugin.sendMessage(sender, Lang.JAIL_SET.get().replace("{jail}", jail));
		return true;
	}
}
