package net.pl3x.bukkit.pl3xcommands.commands;

import java.util.List;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;
import net.pl3x.bukkit.pl3xcommands.configuration.PlayerConfig;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class SetHome implements TabExecutor {
	private Main plugin;

	public SetHome(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "sethome");
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return null;
		}
		if (args.length == 1) {
			return Home.getMatchingHomeNames((Player) sender, args[0]);
		}
		return null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_COMMAND.get());
			return true;
		}
		if (!sender.hasPermission("command.sethome")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length > 0 && !sender.hasPermission("command.sethome.multi")) {
			plugin.sendMessage(sender, Lang.HOME_NO_MULTI.get());
			return true;
		}
		Player player = (Player) sender;
		PlayerConfig config = PlayerConfig.getConfig(player);
		String home = (args.length > 0) ? args[0] : "home";
		if (home.contains(":")) {
			plugin.sendMessage(sender, Lang.HOME_INVALID_CHARACTERS.get());
			return true;
		}
		int limit = Utils.getLimit(player, "command.sethome.limit");
		if (limit == 0) {
			limit = 1;
		}
		int count = config.getMapList("home").size();
		if (limit > 0 && count >= limit) {
			plugin.sendMessage(sender, Lang.HOME_SET_MAX.get().replace("{limit}", Integer.toString(limit)));
			return true;
		}
		config.setLocation("home." + home, player.getLocation());
		if (args.length > 0) {
			plugin.sendMessage(sender, Lang.HOME_SET.get().replace("{home}", home));
		} else {
			plugin.sendMessage(sender, Lang.HOME_SET.get());
		}
		return true;
	}
}
