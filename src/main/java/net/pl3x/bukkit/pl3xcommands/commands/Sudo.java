package net.pl3x.bukkit.pl3xcommands.commands;

import net.pl3x.bukkit.pl3xcommands.Main;
import net.pl3x.bukkit.pl3xcommands.Utils;
import net.pl3x.bukkit.pl3xcommands.configuration.Lang;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Sudo implements CommandExecutor {
	private Main plugin;

	public Sudo(Main plugin) {
		this.plugin = plugin;
		plugin.registerCommand(this, "sudo");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!sender.hasPermission("command.sudo")) {
			plugin.sendMessage(sender, Lang.ERROR_COMMAND_NO_PERMISSION.get());
			return true;
		}
		if (args.length < 2) {
			plugin.sendMessage(sender, cmd.getDescription());
			return false;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			plugin.sendMessage(sender, Lang.ERROR_PLAYER_NOT_ONLINE.get());
			return true;
		}
		if (target.hasPermission("exempt.sudo")) {
			plugin.sendMessage(sender, Lang.SUDO_EXEMPT.get());
			return true;
		}
		String command = Utils.join(args, " ", 1).trim();
		plugin.sendMessage(sender, Lang.SUDO.get().replace("{command}", command).replace("{player}", target.getName()));
		target.performCommand(command);
		return true;
	}
}
